# Modified Fast Downward Translator

This repository contains a modified Helmert's Fast Downward translation tool, which translates a task to a domain, that is used to find a eventually applicable robust plan.

## Notes & Explanation

### Algorithm 2 Simplifications

Algorithm 2 is implemented in the `find_reversible_events`.

We consider events which have `-1` in preconditions (i.e., a given variable has no precondition) automatically irreversible as more than one reverse plan is needed (Alg. 2 looks for a single reverse plan). Such events would be reversible if and only if for each previous value `x`, there would be a path from `y` to `x`. 

In the method `revert` (algorithm 1 in the paper), we instead of the non-deterministic select of the path (line 5 of Alg. 1) select the shortest path from `y` to `x` (it is sufficient for our benchmark domains).

### Algorithm 3 Emulation

For the purposes mentioned in the paper we emulated algorithm 3 by a method `translate` in `translator.py`.

At first, we evaluate `find_reversible_events` (algorithm 2 in the paper) to determine irreversible and reversible events.

Then, for each reversible variable `v` from `RE`, we build a `eoDTG(v)`, (see the paper) and for each it's value `x`, we define a distance variable `d_v_x` representing the shortest path from the current state to  `(v, x)` in `eoDTG(v)`. We use these variables to modify actions such that they follow the conditions from Theorem 13.

Next, we define a variable `unsafe_count`, which will take values from `0` to `n`, where `n` is the maximum size of domains of the considered variables from `RE`. This variable represent the number of consecutively applied unsafe actions (the `un` value in Alg. 3). 

We modify the initial state such that `unsafe_count` is set to `0` and all `d_v_x` values are set accordingly.

Actions which are achievers for irreversible events are modified to increase the `unsafe_count` value. We use function `incrementing_copies`, which copies a given action for each possible values `n, n+1` the `unsafe_count`might achieve.

Actions which are clobberers for irreversible events are required to have the `d_v_x` higher than the `unsafe_count` (where `(v, x)` is a fact that is in preconditions of the clobberred event). If such an action is also an achiever for any (other) irreversible event, we increase `unsafe_count` (see function `constrained_incrementing_copies`), otherwise `unsafe_count` is set to `0` (see function `constrained_zeroing_copies`).

All other actions are modified to preserve the `unsafe_count` if it is `0` and increase it otherwise (as the action is part of an unsafe bridge). 

Reversible events, which are later removed from the generated plan, are encoded as specific actions. They require the `unsafe_count` equal to `0` (in their preconditions) and change the `d_v_x` for each reversible variable `v`, the event modifies, and each its value `x` accordingly.

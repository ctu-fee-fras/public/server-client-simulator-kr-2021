# Planning Server Client Tool

## Introduction
This tool aims to create two binaries (`server` & `client`), which will be able to measure planning and reasoning, especially in stochastic planning domains. 

The server hosts a problem for many possible clients. If a new client contacts the server, it will send him a state. Then the client is supposed to reply, and the whole process is repeated. After each client action, the server may perform several effects on the environment that affect the state. (Client is receiving state with such changes.)

## Dependencies
In order to properly run the simulator, you will have to insert built [Fast Downward](http://www.fast-downward.org/) planner into the `./downward/` folder or change the relative path to an existing build in file `./simulator/include/paths.h`. In order to be able to use `FONDAgent`, [PRP](https://github.com/QuMuLab/planner-for-relevant-policies/tree/master) planner inside folder `./PRP` is required.

## Repository
Repository is containing simulator (contained in `./simulator`) and some other tools. Running binaries `server` & `client` is recommended from `./simulator` folder. **Read `./simulator/README.md` for more information.**

Folder `./domains` contains some PDDL domains used while developing & evaluating algorithms for the paper and can serve as a sufficient example.

Folders `./pddl-unsafeness-limit-translator`, `./toflatfond`, `./pddl-safe-states-finder` (and `./downward`) contains tools used by `LimitAgent`, `AwareAgent` and `APPAgent` and are necessary for correct running of simulator.

Folder `./toFOND` contains tool used by `FONDAgent`. Note that this agent is using [PRP](https://github.com/QuMuLab/planner-for-relevant-policies/tree/master). 

* directory `./downward` should contain build of [Fast Downward](http://www.fast-downward.org/) planner
* directory `./PRP` should contain build of [PRP](https://github.com/QuMuLab/planner-for-relevant-policies/tree/master)

If Fast Downward or PRP are in other directories, the paths can be updated in `include/paths.h` (`FD_PATH` and `PRP_PATH` respectively)

## Running Benchmarks

Benchmarks can be run by `./run-bench.sh <domain> <problem-number> <agent> <repetitions>`.

For example: `./run-bench.sh auv 1 app 10` 

The `evaluate-results.sh` produces the LaTeX table (similar to those in the paper). Note that it might be necessary to amend `PROBLEM_COUNT`, `DOMAINS` and `AGENTS` variables. 

## Missing features & plans
* agent setting parser
* find out why `AwareAgent`'s and `LimitAgent`'s reference plans over the same problem differ

## Known bugs
* `LimitAgent` prints nonreasonable validating time with the new validation
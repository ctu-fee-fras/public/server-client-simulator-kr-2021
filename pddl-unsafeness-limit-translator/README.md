# Translator

Translator used for adding limiting unsafeness variable into PDDL problems.

## Usage

To use translator run `translator.py <domain_file> <problem_file> <safe_states> <max-unsafe-actions> [--translate-events-action] [--add-events-as-operators] [other-fast-downward-translator-options]`.
* `<domain_file>` path to domain file,
* `<problem_file>` path to problem file,
* `<safe_states>` path to safe states file,
* `<max-unsafe-actions>` integer limit of unsafe actions,
* `[-h]` for display script help,
* `[--translate-events-action]` for not skipping translating SASOperator with name `"(events )"`,
* `[--add-events-as-operators]` for adding PDDL events into SAS operators,
* other FastDownward translator options can be also used.

## Safe States File Format

Safe states file has to be in DNF format with conjunction strictly on one line (specifing safe states) and disjunction between lines (delimiting safe states). 

Example:
<code>
    (at car garage) (sunny) (not (out-of-fuel car))
    (at car car-repair-shop)
</code>
This describe two safe states. First one is defined by a car in garage AND sunny weather AND the car is NOT out of fuel. Second one is defined by car in repair shop.

## run_planner.sh script

To use this script, put Fast Downward into `./planner/` folder. Leave `./fast_downward/` where it is and unchanged.  
Usage: `./run_planner.sh <same-arguments-as-translator>`.
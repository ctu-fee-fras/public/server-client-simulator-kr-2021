#!/bin/bash
#$1 - domain $2 - problem number $3 - agent $4 attempts

if [ $3 == "fond" ]
then
DOMAIN="../../domains/$1/domain-nn.pddl"
else
DOMAIN="../domains/$1/domain.pddl"
fi
PROBLEM="../domains/$1/problem$2.pddl"
SAFE_STATES="../../Domains/$1/safe$2.txt"

RES_LOG="../results/res-$1-$2-$3"

cd simulator

MAX_TRIES=$4
COUNT=0
rm $RES_LOG-r*
#touch $RES_LOG
while [  $COUNT -lt $MAX_TRIES ]; do
   ./server $DOMAIN $PROBLEM > /dev/null &
   sleep 1
   ./client -a $3 -S $SAFE_STATES $DOMAIN $PROBLEM > "$RES_LOG-r$COUNT.log"
   #if [ $? -ne 0 ];then
   #   echo "Failed"
   #   echo $COUNT
   #   exit 1
   #fi
   sleep 1
   pkill -9 server
   sleep 1
   let COUNT=COUNT+1
done
echo "Done."
exit 0

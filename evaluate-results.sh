#!/bin/bash

PROBLEM_COUNT=8

AGENTS="app limit fond"
DOMAINS="perestroika"


for ((p=1;p<=$PROBLEM_COUNT;p++)) 
do
printf "$p"
for d in $DOMAINS
do
for a in $AGENTS 
do
COUNT=0
SUCC=0
STEPS=0
REF_LENGTH=0
PLAN_TIME=0

for l in `ls results/res-$d-$p-$a-r*.log`
do
let COUNT=COUNT+1
grep -q "Goal achieved" $l
if [[ $? -eq 0 ]] 
then
let SUCC=SUCC+1
if [ $a == "fond" ]
then
q=1
else
q=2
fi
res=`tail -n $q $l`
let STEPS=STEPS+`echo $res | awk -F ',' '{print $1}' |tr -d [:space:]`
if [ $a == "fond" ]
then
let PLAN_TIME=PLAN_TIME+`echo $res | awk -F ',' '{print $4}' |tr -d [:space:]`
else
let REF_LENGTH=REF_LENGTH+`echo $res | awk -F ',' '{print $3}' |tr -d [:space:]`
let PLAN_TIME=PLAN_TIME+`echo $res | awk -F ',' '{print $5}' |tr -d [:space:]`
fi
fi
done 
if [[ $COUNT -eq $SUCC ]]
then
r1=`echo "scale=0; $PLAN_TIME / $COUNT" | bc -l`
r2=`echo "scale=0; $REF_LENGTH / $COUNT" | bc -l`
r3=`echo "scale=0; $STEPS / $COUNT" | bc -l`
if [ $a == "fond" ]
then
printf " & $r1 & $r3" 
else
printf " & $r1 & $r2 & $r3"
fi 
else
printf "& \multicolumn{3}{|c|}{FAIL}" 
fi
done
done
echo "\\\\"
done

#include "agent.hh"

void Agent::show_statistics(unsigned int actions, double overall_duration) const
{
    std::cout << "Statistics: " << std::endl;
    std::cout << "number of actions, overall_duration (s)" << std::endl;
    std::cout << actions << ", " << overall_duration << std::endl;
}
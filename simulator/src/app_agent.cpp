#include "app_agent.hh"
#include "agents_setting.h"

APPAgent::APPAgent() : timestamp_id(std::time(nullptr))
{

    data_dir = "../tmp/app_agent/" + std::to_string(timestamp_id) + "/";
    std::filesystem::create_directories(data_dir);

    flat_domain_filepath = data_dir + "flat-domain.pddl";
    flat_problem_filepath = data_dir + "flat-partial_problem.pddl";
    partial_problem_filepath = data_dir + "partial_problem.pddl";
    reversible_domain_plan_filepath = data_dir + "pult_plan";
    flat_plan_filepath = data_dir + "flat_plan";
    original_plan_filepath = data_dir + "original_plan";
    flat_subplan_filepath = data_dir + "flat_subplan";
    validation_result_filepath = data_dir + "validation_result";
    log_filepath = data_dir + "log.txt";
}

void APPAgent::init(std::string domain_filepath, std::string problem_filepath)
{
    if (!init_done)
    {

        std::cout << "Preprocessing started." << std::endl;

        auto start = std::chrono::high_resolution_clock::now();

        init_done = true;
        original_domain_path = domain_filepath;
        original_problem_path = problem_filepath;
        if (!generate_plan() || !retranslate_plan() || !load_plan() || !generate_flat_plan())
        {
            std::cerr << "APPAgent encountered some problems and failed." << std::endl;
        }

        auto end = std::chrono::high_resolution_clock::now();
        preprocess_consumed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

        std::cout << "Preprocessing done." << std::endl;
    }
}

std::string APPAgent::act(const SAS_State &current_state, std::string domain_filepath, std::string problem_filepath, std::string safe_states_filepath)
{

    // APP agent doesn't use safe states
    safe_states_filepath = "";

    if (!init_done)
    {
        init(domain_filepath, problem_filepath);
    }

    std::string selected_action;
    if (plan_found && plan_loaded)
    {
        if (!safe_sequence.empty())
        {
            selected_action = safe_sequence.front();
            remove_next_action();
            std::cout << selected_action << std::endl;
        }
        else
        {
            find_safe_sequence(current_state);
            if (safe_sequence.empty())
            {
                selected_action = "(<noop-action>)";
                noops_performed += 1;
            }
            else
            {
                std::cout << "New safe sequence of length " << safe_sequence.size() << "." << std::endl;
                selected_action = safe_sequence.front();
                remove_next_action();
                std::cout << selected_action << std::endl;
            }
        }
        return selected_action;
    }
    else
    {
        throw client_surrender(); // return failure
    }
}

bool APPAgent::generate_flat_domain_and_problem(const SAS_State &current_state)
{

    std::filesystem::create_directories(data_dir);

    current_state.get_problem()->to_strips_file(partial_problem_filepath, current_state, current_state.get_problem()->get_goal());

    char command[1000];
    sprintf(command, "python3 %stranslator.py %s %s >> %s 2>&1 && mv %sflat_domains/* %s", TFFT_PATH, original_domain_path.c_str(), partial_problem_filepath.c_str(), log_filepath.c_str(), TFFT_PATH, data_dir.c_str());
    int r = std::system(command);
    if (r != 0)
    {
        std::cerr << "APPAgent could not generate a flat domain. Error code " << r << " recieved." << std::endl;
        return false;
    }
    else
    {
        return true;
    }
}

bool APPAgent::generate_reversible_domain()
{

    std::filesystem::create_directories(data_dir);

    char command[1000];
    sprintf(command, "python3 %stranslator.py %s %s >> %s 2>&1 && mv %s/output.sas %s", REVT_PATH, original_domain_path.c_str(), original_problem_path.c_str(), log_filepath.c_str(), REVT_PATH, data_dir.c_str());
    int r = std::system(command);
    if (r != 0)
    {
        std::cerr << "APPAgent could not generate a reversible problem. Error code " << r << " recieved." << std::endl;
        return false;
    }
    else
    {
        return true;
    }
}

bool APPAgent::generate_plan()
{

    std::cout << "Generating plan started." << std::endl;

    std::filesystem::create_directories(data_dir);
    DownwardDriver planner(APP_AGENT_SEARCH_SETTING, ">> " + log_filepath);

    bool generating_succeeded = generate_reversible_domain();
    if (!generating_succeeded)
    {
        return false;
    }

    int search_result = planner.plan_sas(data_dir + "output.sas", reversible_domain_plan_filepath, APP_AGENT_SEARCH_TIME_LIMIT);

    if (search_result == 0)
    {
        plan_found = true;
    }
    else
    {
        std::cerr << "APPAgent could not generate a plan with FastDownward. Error code " << search_result << " recieved." << std::endl;
        plan_found = false;
    }

    std::cout << "Generating plan ended." << std::endl;

    return plan_found;
}

bool APPAgent::retranslate_plan()
{

    std::cout << "Plan retranslating started." << std::endl;

    // remove events and comments at first, then create new file with without retranslated unsafe events
    char command[500];
    sprintf(command, "sed -i -e '/;/d' -e '/(events/d' -e '/event-action-/d' %s && sed -e 's/-inc-[[:digit:]]\\+-[[:digit:]]\\+\\s/ /g' -e 's/-req-0\\s/ /g' -e 's/-constrained-zeroing-copy\\s/ /g' -e 's/-constrained-inc-copy\\s/ /g' -e 's/-inc-copy-[[:digit:]]\\+-[[:digit:]]\\+\\s/ /g' %s > %s", reversible_domain_plan_filepath.c_str(), reversible_domain_plan_filepath.c_str(), original_plan_filepath.c_str());
    int r = std::system(command);

    std::cout << "Plan retranslating ended." << std::endl;

    if (r != 0)
    {
        std::cerr << "APPAgent could not retranslate the plan. Error code " << r << " recieved." << std::endl;
        return false;
    }
    else
    {
        return true;
    }
}

bool APPAgent::load_plan()
{

    std::cout << "Plan loading started." << std::endl;

    plan.clear();
    std::ifstream file(original_plan_filepath);
    if (!file)
    {
        std::cerr << "APPAgent could not open file '" << original_plan_filepath << "'." << std::endl;
        return false;
    }
    std::string line;
    while (std::getline(file, line))
    {
        line = line.substr(0, line.find_first_of(';'));
        if (line.size() > 0)
        {
            plan.push_back(line);
        }
    }
    file.close();

    pult_plan.clear();
    std::ifstream pult_file(reversible_domain_plan_filepath);
    if (!pult_file)
    {
        std::cerr << "APPAgent could not open file '" << reversible_domain_plan_filepath << "'." << std::endl;
        return false;
    }
    std::string pult_line;
    while (std::getline(pult_file, pult_line))
    {
        pult_line = pult_line.substr(0, pult_line.find_first_of(';'));
        if (pult_line.size() > 0)
        {
            pult_plan.push_back(pult_line);
        }
    }
    pult_file.close();

    plan_loaded = true;

    reference_plan_lenght = plan.size();

    std::cout << "Plan loading ended." << std::endl;

    return true;
}

void APPAgent::find_safe_sequence(const SAS_State &current_state, bool shortest)
{

    auto start = std::chrono::high_resolution_clock::now();
    safe_sequence.clear();
    if (plan_found && plan_loaded)
    {

        std::vector<std::unordered_set<size_t>> p_plus(current_state.get_assignment().size());
        std::vector<std::unordered_set<size_t>> p_minus(current_state.get_assignment().size());
        SAS_State s_i = current_state;
        size_t plan_size = plan.size();

        size_t safe_sequence_lenght = 0;

        /* THIS DOESN'T WORK WITH CONDITIONAL EFFECTS */

        for (size_t i = 0; i < plan_size; i += 1)
        {

            const SAS_Action &a = current_state.get_problem()->find_action_by_name(plan.at(i).substr(1, plan.at(i).size() - 2));

            /* check action applicability */
            if (!s_i.is_applicable(a))
            {
                break;
            }
            else
            {
                bool violation = false;
                for (const auto &p : a.get_preconditions())
                {
                    if (p_minus[p.first].find(p.second) != p_minus[p.first].end())
                    {
                        violation = true;
                        if (shortest)
                        {
                            break;
                        }
                    }
                }
                if (violation)
                {
                    break;
                }
                else
                {
                    if (!is_unsafe(pult_plan.at(i)))
                    {
                        safe_sequence_lenght = i + 1;
                    }
                }
            }

            /* find applicable events */

            auto event_state = p_plus;
            for (const auto &p : s_i.get_assignment())
            {
                event_state[p.first].insert(p.second);
            }
            for (const auto &p : a.get_grounded_effects(s_i))
            {
                event_state[p.first].clear();
                event_state[p.first].insert(p.second);
            }

            std::vector<std::reference_wrapper<const SAS_Event>> e_i;

            for (const SAS_Event &e : current_state.get_problem()->get_events())
            {
                bool contains_all = true;
                for (const auto &p : e.get_preconditions())
                {
                    if (event_state[p.first].find(p.second) == event_state[p.first].end())
                    {
                        contains_all = false;
                        break;
                    }
                }
                if (contains_all)
                {
                    e_i.push_back(e);
                }
            }

            /* change plus_i sets to plus_i+1 */

            for (const auto &p : a.get_grounded_effects(s_i))
            {

                p_plus[p.first].clear();
                p_plus[p.first].insert(p.second);

                p_minus[p.first].clear();
                size_t range_size = current_state.get_problem()->get_variables().at(p.first).get_range();
                for (size_t value = 0; value < range_size; value += 1)
                {
                    if (value != p.second)
                    {
                        p_minus[p.first].insert(value);
                    }
                }
            }

            for (const SAS_Event &e : e_i)
            {
                for (const auto &p : e.get_grounded_effects(s_i))
                {

                    p_plus[p.first].insert(p.second);

                    size_t range_size = current_state.get_problem()->get_variables().at(p.first).get_range();
                    for (size_t value = 0; value < range_size; value += 1)
                    {
                        if (value != p.second)
                        {
                            p_minus[p.first].insert(value);
                        }
                    }
                }
            }

            /*  change state */

            s_i.apply(a);
        }

        for (size_t i = 0; i < safe_sequence_lenght; i += 1)
        {
            safe_sequence.push_back(plan[i]);
        }
    }
    else
    {
        std::cerr << "APPAgent cannot find safe sequence when plan is not known." << std::endl;
    }

    auto end = std::chrono::high_resolution_clock::now();
    validating_robust_plan_consumed += std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
}

bool APPAgent::generate_flat_plan()
{
    std::string command = "sed -e 's/(/(events)\\n(/g' " + original_plan_filepath + " | sed 1d > " + flat_plan_filepath;
    int r = std::system(command.c_str());
    if (r != 0)
    {
        std::cerr << "APPAgent could not generate a flat plan. Error code " << r << " recieved." << std::endl;
        return false;
    }
    else
    {
        return true;
    }
}

void APPAgent::remove_next_action()
{
    if (!safe_sequence.empty())
    {
        safe_sequence.pop_front();
        plan.pop_front();
        pult_plan.pop_front();
    }
}

bool APPAgent::is_unsafe(std::string pddl_action)
{
    std::string unsafe_suffix("-inc-copy");
    std::size_t found = pddl_action.find(unsafe_suffix);
    return found != std::string::npos;
}

APPAgent::~APPAgent()
{
    if (remove_temp_files)
    {
        std::cout << "Removing temp files." << std::endl;
        if (std::filesystem::remove_all(data_dir) == 0)
        {
            std::cerr << "Temporary files inside " << data_dir << " cannot be deleted." << std::endl;
        }
    }
}

void APPAgent::show_statistics(unsigned int actions, double overall_time) const
{
    std::cout << "Statistics:" << std::endl;
    std::cout << "number of actions, overall_duration (s), reference plan lenght, number of noops, preprocess consumed (ms), validating consumed (ms)" << std::endl;
    std::cout << actions << ", " << overall_time << ", " << reference_plan_lenght << ", " << noops_performed << ", " << preprocess_consumed.count() << ", " << validating_robust_plan_consumed.count() << std::endl;
}
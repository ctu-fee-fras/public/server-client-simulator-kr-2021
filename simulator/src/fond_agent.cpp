#include "fond_agent.hh"

std::string FONDAgent::act(const SAS_State &current_state, std::string domain_filepath, std::string problem_filepath, std::string safe_states_filepath)
{
    safe_states_filepath = "";
    init(domain_filepath, problem_filepath);
    std::string selected_action = find_action(current_state);
    return selected_action;
}

FONDAgent::FONDAgent() : timestamp_id(std::time(nullptr))
{

    data_dir = "../tmp/fond_agent/" + std::to_string(timestamp_id) + "/";
    std::filesystem::create_directories(data_dir);

    fond_domain = data_dir + "fond_domain.pddl";
    fond_problem_filepath = data_dir + "fond_problem.pddl";
    policy_filepath = data_dir + "policy.sas";
    translated_policy_filepath = data_dir + "translated_policy.sas";

    remove_temp_files = FOND_AGENT_DELETE_TEMP_FILES;

    log_filepath = data_dir + "log.txt";
}

FONDAgent::~FONDAgent()
{
    if (remove_temp_files)
    {
        std::cout << "Removing temp files." << std::endl;
        if (std::filesystem::remove_all(data_dir) == 0)
        {
            std::cerr << "Temporary files inside " << data_dir << " cannot be deleted." << std::endl;
        }
    }
}

void FONDAgent::init(std::string domain_filepath, std::string problem_filepath)
{

    if (!init_done)
    {

        std::filesystem::remove("./output.sas");
        std::filesystem::remove("./output");

        std::cout << "Preprocessing started." << std::endl;

        auto start = std::chrono::high_resolution_clock::now();

        init_done = true;

        if (!translate_to_FOND(domain_filepath, problem_filepath) || !find_policy() || !load_fond_problem() || !translate_strategy())
        {
            std::cerr << "FONDAgent encountered some problems and failed." << std::endl;
        }

        auto end = std::chrono::high_resolution_clock::now();
        preprocess_consumed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

        std::cout << "Preprocessing done." << std::endl;
    }
}

bool FONDAgent::translate_to_FOND(std::string domain_filepath, std::string problem_filepath)
{

    char command[1000];
    sprintf(command, "python3 %stoFOND2.py %s %s %s %s >> %s 2>&1", TOFOND_PATH, domain_filepath.c_str(), problem_filepath.c_str(), fond_domain.c_str(), fond_problem_filepath.c_str(), log_filepath.c_str());

    int r = std::system(command);
    if (r != 0)
    {
        std::cerr << "FONDAgent could not generate a fond problem. Error code " << r << " recieved." << std::endl;
        return false;
    }
    else
    {
        return true;
    }
}

bool FONDAgent::find_policy()
{

    std::filesystem::remove("./policy.out");

    char command[1000];
    sprintf(command, "prlimit --as=%ld --cpu=%d %ssrc/prp %s %s --dump-policy 2 >> %s 2>&1", FOND_AGENT_SEARCH_MEMORY_LIMIT, FOND_AGENT_SEARCH_CPU_TIME_LIMIT, PRP_PATH, fond_domain.c_str(), fond_problem_filepath.c_str(), log_filepath.c_str());

    int r = std::system(command);

    if (!std::filesystem::exists("./policy.out"))
    {
        std::cerr << "FONDAgent could not find a policy. Planning failed. Error code " << WEXITSTATUS(r) << " recieved." << std::endl;
        return false;
    }
    else
    {
        std::filesystem::copy("./policy.out", policy_filepath);
        std::filesystem::copy("./policy.fsap", data_dir + "./policy.fsap");
        std::filesystem::copy("./sas_plan", data_dir + "./sas_plan");
        std::filesystem::copy("./elapsed.time", data_dir + "./elapsed.time");
        std::filesystem::copy("./output.sas", data_dir + "./output.sas");
        std::filesystem::copy("./output", data_dir + "./output");
        std::filesystem::copy("./plan_numbers_and_cost", data_dir + "./plan_numbers_and_cost");
        std::filesystem::remove("./policy.out");
        std::filesystem::remove("./policy.fsap");
        std::filesystem::remove("./sas_plan");
        std::filesystem::remove("./elapsed.time");
        std::filesystem::remove("./output.sas");
        std::filesystem::remove("./output");
        std::filesystem::remove("./plans_numbers_and_cost");
        return true;
    }
}

std::string FONDAgent::negate_strips(const std::string &strips_action)
{
    if (strips_action.substr(0, 4) == "(not")
    {
        return strips_action.substr(5, strips_action.size() - 6);
    }
    else
    {
        return "(not " + strips_action + ")";
    }
}

bool FONDAgent::contains_strips(const std::string &strips_action, const std::string &string)
{
    if (strips_action.substr(0, 4) == "(not")
    {
        return string.find(strips_action) != std::string::npos;
    }
    else
    {
        return string.find(strips_action) != std::string::npos && string.find(this->negate_strips(strips_action)) == std::string::npos;
    }
}

std::string FONDAgent::find_action(const SAS_State &state)
{

    std::ifstream f(translated_policy_filepath);
    if (!f.is_open())
    {
        std::cerr << "FOND Agent encoutered a problem while reading action." << std::endl;
        throw client_surrender();
    }

    std::string line;
    bool target_found = false;
    while (std::getline(f, line))
    {

        if (line == "" || (!target_found && line.find("Execute:") != std::string::npos))
        {
            continue;
        }

        if (target_found)
        {
            f.close();
            std::string action = "(" + line.substr(9, line.find_first_of(' ', 9) - 9) + ")";
            std::replace(action.begin(), action.end(), '_', ' ');
            try
            {
                state.get_problem()->find_action_by_name(action.substr(1, action.size() - 2));
            }
            catch (const SAS_not_found &e)
            {
                target_found = false;
                continue;
            }
            std::cout << action << std::endl;
            return action;
        }

        bool violated = false;
        for (auto p : state.get_assignment())
        {
            const SAS_Variable_Info &variable = state.get_problem()->get_variables().at(p.first);
            size_t range = state.get_problem()->get_variables().at(p.first).get_range();
            std::string strips_value = variable.to_strips_string_atom(p.second);
            if (contains_strips(strips_value, line))
            {
                continue;
            }
            else if (contains_strips(negate_strips(strips_value), line))
            {
                violated = true;
                break;
            }
            for (size_t value = 0; value < range; value += 1)
            {
                if (value != p.second)
                {
                    std::string other_strips_value = variable.to_strips_string_atom(value);
                    if (contains_strips(other_strips_value, line))
                    {
                        if (strips_value.substr(0, 4) == "(not ")
                        {
                            continue;
                        }
                        else
                        {
                            violated = true;
                            break;
                        }
                    }
                    std::string negated_other_strips_value = negate_strips(variable.to_strips_string_atom(value));
                    if (contains_strips(negated_other_strips_value, line))
                    {
                        if (strips_value.substr(0, 4) == "(not ")
                        {
                            violated = true;
                            break;
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
            if (violated)
            {
                break;
            }
        }

        if (!violated)
        {
            target_found = true;
        }
    }

    f.close();

    number_of_noops += 1;
    return "(<noop-action>)";
}

bool FONDAgent::load_fond_problem()
{
    SAS_Parser parser;
    fond_problem = std::shared_ptr<const SAS_Problem>(new SAS_Problem(parser.parse(data_dir + "./output.sas")));
    return true;
}

std::string FONDAgent::find_corresponding_action(const std::string &variable_name, int value)
{
    for (const auto &variable : fond_problem->get_variables())
    {
        if (variable.get_name() == variable_name)
        {
            std::string action = variable.to_strips_string_atom(value);
            std::replace(action.begin(), action.end(), '_', ' ');
            return action;
        }
    }
    throw SAS_not_found("No variable with such name.");
}

bool FONDAgent::translate_strategy()
{

    std::ifstream policy_file(policy_filepath);
    if (!policy_file.is_open() || !policy_file.good())
    {
        std::cerr << "Reading policy filepath cannot be opened." << std::endl;
        return false;
    }

    std::string strategy_text;
    policy_file.seekg(0, std::ios::end);
    strategy_text.reserve(policy_file.tellg());
    policy_file.seekg(0, std::ios::beg);
    strategy_text.assign((std::istreambuf_iterator<char>(policy_file)), std::istreambuf_iterator<char>());
    policy_file.close();

    std::set<std::string> mentioned_variables;

    std::regex e("(var[0-9]+:[0-9]+)");
    std::copy(std::sregex_token_iterator(strategy_text.begin(), strategy_text.end(), e, 0), std::sregex_token_iterator(), std::inserter(mentioned_variables, mentioned_variables.begin()));

    for (const auto &text_assignment : mentioned_variables)
    {
        size_t index = 0;
        const auto delimiter = text_assignment.find(":");
        std::string new_text_assignment = find_corresponding_action(text_assignment.substr(0, delimiter), std::stoul(text_assignment.substr(delimiter + 1)));
        while ((index = strategy_text.find(text_assignment, index)) != std::string::npos)
        {
            strategy_text.replace(index, text_assignment.length(), new_text_assignment);
            index += new_text_assignment.length();
        }
    }

    std::ofstream new_policy_file(translated_policy_filepath);
    if (!new_policy_file.is_open() || !new_policy_file.good())
    {
        std::cerr << "Reading policy filepath cannot be opened." << std::endl;
        return false;
    }

    new_policy_file << strategy_text;
    new_policy_file.close();

    return true;
}

void FONDAgent::show_statistics(unsigned int actions, double overall_time) const
{
    std::cout << "Statistics:" << std::endl;
    std::cout << "number of actions, overall_duration (s), number of noops, preprocess consumed (ms)" << std::endl;
    std::cout << actions << ", " << overall_time << ", " << number_of_noops << ", " << preprocess_consumed.count() << std::endl;
}
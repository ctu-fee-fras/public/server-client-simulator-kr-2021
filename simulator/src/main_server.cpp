#include <getopt.h>

#include <cstdlib>
#include <iostream>
#include <string>

#include "config.hh"
#include "server.h"
#include "utility.h"
#include "options.hpp"
#include "exceptions.hpp"
#include "exit_codes.h"

#include "SAS_parser.hpp"
#include "SAS_problem.hpp"

using namespace std;

void usage(char *filename);

int main(int argc, char *argv[])
{

    ServerConfig config;

    try
    {

        config = get_server_config(argc, argv);
    }
    catch (not_enought_arguments &e)
    {

        std::cerr << e.what() << std::endl;
        return SERVER_NOT_ENOUGH_ARGUMENTS;
    }
    catch (wrong_argument &e)
    {

        std::cerr << e.what() << std::endl;
        return SERVER_WRONG_ARGUMENT;
    }
    catch (...)
    {

        std::cerr << "Unknown exception caught." << std::endl;
        return SERVER_UNKNOWN_EXCEPTION;
    }

    std::string event_prefix = "event-action-prefix-";

    // TODO: consider simplyfing translating and parsing into one call SAS_Parser.parse(domain, problem)

    // TODO: both client and server are translating problem into a file with the same name
    //       this may potentially cause override, especially when translating takes a while

    std::cout << "Translating " << argv[optind] << " and " << argv[optind + 1] << " into output.sas.. ";
    int r = fd_translate_with_events(event_prefix, argv[optind], argv[optind + 1]);
    if (r == 0)
    {
        std::cout << "ok! :)" << std::endl;
    }
    else
    {
        std::cout << "error! :(" << std::endl;
        std::cerr << "Fast Downward translating failed. :(" << std::endl;
        return SERVER_TRANSLATION_FAILED;
    }

    SAS_Parser sas_parser;
    SAS_Problem sas_problem = sas_parser.parse("./output.sas", event_prefix);

    int remove_return = system("rm ./output.sas");
    if (remove_return != 0 || WEXITSTATUS(remove_return) != 0)
    {
        cerr << "Warning. Removing temporary file './output.sas' failed." << endl;
    }

    config.set_sas_problem(&sas_problem);

    int server_return = server(config);

    // see 'exit_codes.h' for more information
    return server_return;
}
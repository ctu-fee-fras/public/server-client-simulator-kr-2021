#include "options.hpp"

ClientConfig get_client_config(int argc, char *argv[])
{

    ClientConfig config;

    // parameter treatments
    struct option long_options[] = {{"port", required_argument, 0, 'p'},
                                    {"host", required_argument, 0, 'h'},
                                    {"agent", required_argument, 0, 'a'},
                                    {"safe-states", required_argument, 0, 'S'},
                                    {0, 0, 0, 0}};

    const char option_string[] = "p:h:a:S:";

    // not enough arguments (not the path to the domain + the path to the problem)
    if (argc < 3)
    {
        std::string error("Not enought arguments.");
        throw not_enought_arguments(error);
    }

    int c = 0;

    // get command line options
    while (c != -1)
    {

        int option_index = 0;
        c = getopt_long(argc, argv, option_string, long_options, &option_index);
        if (c == -1)
        {
            break;
        }

        if (c == 'a')
        {
            config.set_agent_id(optarg);
        }
        else if (c == 'h')
        {
            config.set_server_ip(optarg);
        }
        else if (c == 'p')
        {
            config.set_port(atoi(optarg));
        }
        else if (c == 'S')
        {
            config.set_safe_states(optarg);
        }
        else
        {
            std::string error("Wrong argument " + std::to_string(c) + " passed.");
            throw wrong_argument(error);
        }
    };

    // not enough arguments (not the path to the domain + the path to the problem)
    if (argc - optind != 2)
    {
        std::string error("Not enought arguments.");
        throw not_enought_arguments(error);
    }

    config.set_domain_directory(argv[optind]);
    config.set_problem_directory(argv[optind + 1]);

    return config;
}

ServerConfig get_server_config(int argc, char *argv[])
{

    ServerConfig config;

    // parameter treatments
    struct option long_options[] = {{"port", required_argument, 0, 'p'},
                                    {"turn limit", required_argument, 0, 'T'},
                                    {"time limit", required_argument, 0, 't'},
                                    {0, 0, 0, 0}};

    const char option_string[] = "p:T:t:";

    // not enough arguments (not the path to the domain + the path to the problem)
    if (argc < 3)
    {
        std::string error("Not enought arguments.");
        throw not_enought_arguments(error);
    }

    int c = 0;

    // get command line options
    while (c != -1)
    {

        int option_index = 0;
        c = getopt_long(argc, argv, option_string, long_options, &option_index);
        if (c == -1)
        {
            break;
        }

        if (c == 'T')
        {
            config.set_turn_lim(atoi(optarg));
        }
        else if (c == 't')
        {
            config.set_time_lim(std::stof(optarg));
        }
        else if (c == 'p')
        {
            config.set_port(atoi(optarg));
        }
        else
        {
            std::string error("Wrong argument passed.");
            throw wrong_argument(error);
        }
    };

    // not enough arguments (not the path to the domain + the path to the problem)
    if (argc - optind != 2)
    {
        std::string error("Not enought arguments.");
        throw not_enought_arguments(error);
    }

    return config;
}

void server_usage(char *filename)
{
    std::cout << "usage: " << filename << " [options] <domain.pddl> <problem.pddl>" << std::endl;
    std::cout << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << " --port\t\t-p number\tport (default 2222)." << std::endl;
    std::cout << " --turn limit\t-T number\tmaximum of turns (default 1000)." << std::endl; // TODO. add dynamic defaults
    std::cout << " --time limit\t-t number\tmaximum of turns (default 1000)." << std::endl; // TODO. add dynamic defaults
}

void client_usage(char *filename)
{
    std::cout << "usage: " << filename << " [options] <domain.pddl> <problem.pddl>" << std::endl;
    std::cout << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << " --port\t\t-p port\t\t\thost port (default 2222)" << std::endl;  //TODO: add dynamic default
    std::cout << " --host\t\t-h ip\t\t\thost ip (default localhost)" << std::endl; //TODO: add dynamic default
    std::cout << " --agent\t-a <limit|aware|app|fond>\tname of agent to use in reasoning" << std::endl;
    std::cout << " --safe-states\t-S filepath\t\tfilepath to a file containing safe states description (in CNF)" << std::endl;
}
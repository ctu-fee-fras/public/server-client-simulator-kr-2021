#include "client.h"

int client(ClientConfig &conf)
{

    int r = 0;
    bool run = true;
    unsigned int turn = 0;

    std::string host = conf.get_server_ip();
    unsigned int port = conf.get_port();

    int socket = connect(host.c_str(), port);
    if (socket <= 0)
    {
        std::cerr << "Connection to " << host << ':' << port << " failed." << std::endl;
        return CLIENT_CONNECTION_FAILED;
    }

    std::shared_ptr<Agent> agent = select_agent(conf.get_agent_id());
    SAS_State current_state = conf.get_sas_problem()->get_init();

    while (run && r == 0)
    {

        turn++;

        r = recieve_state(socket, current_state, run, agent);
        if (r != 0 || !run)
        {
            break;
        }

        std::string action;
        try
        {
            action = agent->act(current_state, conf.get_domain_directory(), conf.get_problem_directory(), conf.get_safe_states());
        }
        catch (client_surrender &e)
        {
            std::cout << "Client surrendered." << std::endl;
            send_surrender(socket);
            r = CLIENT_SURRENDER;
            break;
        }

        r = send_action(socket, action.substr(1, action.size() - 2));
    }

    close(socket);
    return r;
}

unsigned int recieve_state(int socket, SAS_State &current_state, bool &run, const std::shared_ptr<const Agent> &agent)
{

    unsigned int message_lenght;
    if (!read(socket, &message_lenght, sizeof(unsigned int)))
    {
        std::cerr << "error read stat_len" << std::endl;
        return CLIENT_COMMUNICATION_FAILED;
    }

    std::vector<char> buffer(message_lenght);

    if (!read(socket, buffer.data(), message_lenght * sizeof(char)))
    {
        std::cerr << "error read mess_state" << std::endl;
        return CLIENT_COMMUNICATION_FAILED;
    }

    std::string message(buffer.begin(), buffer.end());

    if (message_is_state(message))
    {

        process_state_message(message, current_state);
    }
    else if (message_is_win(message))
    {

        run = false;
        return process_win_message(message, agent);
    }
    else
    {

        if (message == MESSAGE_OUT_OF_TURNS)
        {
            return CLIENT_OUT_OF_TURNS;
        }
        else if (message == MESSAGE_OUT_OF_TIME)
        {
            return CLIENT_OUT_OF_TIME;
        }
        else if (message == MESSAGE_NONAPPLICABLE_ACTION)
        {
            return CLIENT_NONAPPLICABLE_ACTION;
        }
        else if (message == MESSAGE_BAD_ACTION)
        {
            return CLIENT_BAD_ACTION;
        }
        else
        {
            std::cerr << "Unknow message recieved." << std::endl;
            return CLIENT_COMMUNICATION_FAILED;
        }
    }

    return 0;
}

bool message_is_state(const std::string &message)
{
    return message.size() >= 8 + 7 && message.compare(0, 7, "<state>") == 0 && message.compare(message.size() - 8, 8, "</state>") == 0;
}

bool message_is_win(const std::string &message)
{
    return message.size() >= 5 + 6 && message.compare(0, 5, "<win>") == 0 && message.compare(message.size() - 6, 6, "</win>") == 0;
}

void process_state_message(const std::string &message, SAS_State &current_state)
{

    size_t number_of_variables = (message.size() - 7 - 8) / sizeof(size_t);

    std::vector<size_t> values(number_of_variables);
    memcpy(values.data(), message.c_str() + 7, message.size() - 7 - 8);

    for (size_t i = 0; i < number_of_variables; i += 1)
    {
        current_state.set(i, values[i]);
    }
}

unsigned int process_win_message(const std::string &message, const std::shared_ptr<const Agent> &agent)
{

    int turns;
    double time;

    size_t turn_begin = message.find("<turn>", 5);
    size_t turn_end = message.find("</turn>", turn_begin);

    if (turn_end != std::string::npos && turn_begin != std::string::npos)
    {
        turns = std::stoi(message.substr(turn_begin + 6, turn_end - turn_begin - 6));
    }
    else
    {
        return CLIENT_COMMUNICATION_FAILED;
    }

    size_t time_begin = message.find("<time>", 5);
    size_t time_end = message.find("</time>", time_begin);

    if (time_end != std::string::npos && time_begin != std::string::npos)
    {
        time = std::stod(message.substr(time_begin + 6, time_end - time_begin - 6));
    }
    else
    {
        return CLIENT_COMMUNICATION_FAILED;
    }

    std::cout << "Goal achieved!" << std::endl;
    agent->show_statistics(turns, time);

    return 0;
}

int send_action(const int socket, const std::string &action)
{

    std::string action_message = "<action>" + action + "</action>";

    unsigned int message_lenght = action_message.length();
    if (!write(socket, &message_lenght, sizeof(unsigned int)))
    {
        std::cerr << "Client failed while sending action lenght." << std::endl;
        return CLIENT_COMMUNICATION_FAILED;
    }

    if (!write(socket, action_message.c_str(), message_lenght))
    {
        std::cerr << "Client failed while sending action." << std::endl;
        return CLIENT_COMMUNICATION_FAILED;
    }

    return 0;
}

void send_surrender(const int socket)
{

    std::string message = MESSAGE_SURRENDER;

    unsigned int message_lenght = message.length();
    if (!write(socket, &message_lenght, sizeof(unsigned int)))
    {
        std::cerr << "Client failed while sending surrender message lenght." << std::endl;
    }

    if (!write(socket, message.c_str(), message_lenght))
    {
        std::cerr << "Client failed while sending surrender message." << std::endl;
    }
}

int connect(const char *hostname, unsigned int port)
{
    struct hostent *host = ::gethostbyname(hostname);
    if (!host)
    {
        std::cerr << "ERROR gethostbyname" << std::endl;
        return -1;
    }
    int sock = socket(PF_INET, SOCK_STREAM, 0);
    if (sock == -1)
    {
        std::cerr << "ERROR socket" << std::endl;
        return -1;
    }
    struct sockaddr_in addr;
    addr.sin_addr = *((struct in_addr *)host->h_addr);
    addr.sin_port = htons(port);
    addr.sin_family = AF_INET;

    if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1)
    {
        perror("connect");
        return -1;
    }
    return sock;
}
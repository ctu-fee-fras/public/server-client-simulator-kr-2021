#include "config.hh"

/****************** ServerConfig ***********************/

ServerConfig::ServerConfig() : _turn_limit(__CONFIG_DEFAULT_MAX_TURN), _time_limit(__CONFIG_DEFAULT_MAX_TIME), _port(__CONFIG_DEFAULT_PORT) {}

const SAS_Problem *ServerConfig::get_sas_problem() const
{
  return _problem;
}

unsigned int ServerConfig::get_turn_lim() const
{
  return _turn_limit;
}

float ServerConfig::get_time_lim() const
{
  return _time_limit;
}

unsigned int ServerConfig::get_port() const
{
  return _port;
}

void ServerConfig::set_turn_lim(unsigned int turn_limit)
{
  _turn_limit = turn_limit;
}

void ServerConfig::set_time_lim(float time_limit)
{
  _time_limit = time_limit;
}

void ServerConfig::set_port(unsigned int port)
{
  _port = port;
}

void ServerConfig::set_sas_problem(SAS_Problem *problem)
{
  _problem = problem;
}

/**************** ClientConfig ******************/
ClientConfig::ClientConfig() : _port(__CONFIG_CLIENT_DEFAULT_PORT), _serverip(__CONFIG_CLIENT_DEFAULT_SERVER_IP), _set_cost(true), _agent_id(__CONFIG_DEFAULT_AGENT) {}

const SAS_Problem *ClientConfig::get_sas_problem() const
{
  return _problem;
}

unsigned int ClientConfig::get_port() const
{
  return _port;
}

std::string ClientConfig::get_server_ip() const
{
  return _serverip;
}

std::string ClientConfig::get_domain_directory() const
{
  return _domain_directory;
}

std::string ClientConfig::get_problem_directory() const
{
  return _problem_directory;
}

std::string ClientConfig::get_planner() const
{
  return _planner;
}
std::string ClientConfig::get_safe_states() const
{
  return _safe_states;
}

bool ClientConfig::get_set_cost() const
{
  return _set_cost;
}

std::string ClientConfig::get_agent_id() const
{
  return _agent_id;
}

void ClientConfig::set_port(unsigned int port)
{
  _port = port;
}

void ClientConfig::set_server_ip(const std::string &server_ip)
{
  _serverip = server_ip;
}

void ClientConfig::set_domain_directory(const std::string dd)
{
  _domain_directory = dd;
}

void ClientConfig::set_problem_directory(const std::string pd)
{
  _problem_directory = pd;
}

void ClientConfig::set_planner(const std::string planner)
{
  _planner = planner;
}

void ClientConfig::set_safe_states(const std::string safe_states_filepath)
{
  _safe_states = safe_states_filepath;
}

void ClientConfig::set_set_cost(bool b)
{
  _set_cost = b;
}

void ClientConfig::set_agent_id(std::string id)
{
  _agent_id = id;
}

void ClientConfig::set_sas_problem(SAS_Problem *problem)
{
  _problem = problem;
}

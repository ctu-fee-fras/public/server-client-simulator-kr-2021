#include "aware_agent.hh"

AwareAgent::AwareAgent() : timestamp_id(std::time(nullptr))
{

    data_dir = "../tmp/aware_agent/" + std::to_string(timestamp_id) + "/";
    std::filesystem::create_directories(data_dir);

    flat_domain_filepath = data_dir + "flat-domain.pddl";
    flat_problem_filepath = data_dir + "flat-partial_problem.pddl";
    subproblem_problem_filepath = data_dir + "subproblem_problem.pddl";
    partial_problem_filepath = data_dir + "partial_problem.pddl";
    pult_plan_filepath = data_dir + "pult_plan";
    flat_plan_filepath = data_dir + "flat_plan";
    original_plan_filepath = data_dir + "original_plan";
    flat_subplan_filepath = data_dir + "flat_subplan";
    partial_robust_plan_filepath = data_dir + "partial_plan";
    validation_result_filepath = data_dir + "validation_result";
    log_filepath = data_dir + "log.txt";
}

void AwareAgent::init(const SAS_State &state, std::string domain_filepath, std::string problem_filepath, std::string safe_states_filepath)
{
    if (!init_done)
    {

        std::cout << "Preprocessing started." << std::endl;

        auto start = std::chrono::high_resolution_clock::now();

        init_done = true;
        original_domain_path = domain_filepath;
        original_problem_path = problem_filepath;
        this->safe_states_filepath = safe_states_filepath;
        if (safe_states_filepath == "")
        {
            std::cerr << "AwareAgent is missing <-S|--safe-states> argument." << std::endl;
            return;
        }
        if (!generate_flat_domain_and_problem(state) || !generate_plan() || !retranslate_plan() || !load_plan() || !generate_flat_plan())
        {
            std::cerr << "AwareAgent encountered some problems and failed." << std::endl;
        }

        auto end = std::chrono::high_resolution_clock::now();
        preprocess_consumed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

        std::cout << "Preprocessing done." << std::endl;
    }
}

std::string AwareAgent::act(const SAS_State &state, std::string domain_filepath, std::string problem_filepath, std::string safe_states_filepath)
{

    if (!init_done)
    {
        init(state, domain_filepath, problem_filepath, safe_states_filepath);
    }

    std::string selected_action;
    if (plan_found && plan_loaded)
    {
        if (!safe_sequence.empty())
        {
            selected_action = safe_sequence.front();
            remove_next_action();
            std::cout << selected_action << std::endl;
        }
        else if (!partial_robust_plan.empty())
        {
            selected_action = partial_robust_plan.front();
            partial_robust_plan.pop_front();
            std::cout << selected_action << std::endl;
        }
        else
        {
            find_safe_sequence(state);
            if (safe_sequence.empty())
            {
                if (!generate_robust_plan(state))
                {
                    throw client_surrender();
                }
                if (partial_robust_plan.empty())
                {
                    selected_action = "(<noop-action>)";
                    noops_performed += 1;
                }
                else
                {
                    std::cout << "New robust plan of length " << partial_robust_plan.size() << "." << std::endl;
                    pop_until_next_safe_state();
                    selected_action = partial_robust_plan.front();
                    partial_robust_plan.pop_front();
                    std::cout << selected_action << std::endl;
                }
            }
            else
            {
                std::cout << "New safe sequence of length " << safe_sequence.size() << "." << std::endl;
                selected_action = safe_sequence.front();
                remove_next_action();
                std::cout << selected_action << std::endl;
            }
        }
        return selected_action;
    }
    else
    {
        throw client_surrender(); // return failure
    }
}

bool AwareAgent::generate_flat_domain_and_problem(const SAS_State &state)
{

    std::filesystem::create_directories(data_dir);

    state.get_problem()->to_strips_file(partial_problem_filepath);

    char command[1000];
    sprintf(command, "python3 %stranslator.py %s %s >> %s 2>&1 && mv %sflat_domains/* %s", TFFT_PATH, original_domain_path.c_str(), partial_problem_filepath.c_str(), log_filepath.c_str(), TFFT_PATH, data_dir.c_str());
    int r = std::system(command);
    if (r != 0)
    {
        std::cerr << "AwareAgent could not generate a flat domain. Error code " << r << " recieved." << std::endl;
        return false;
    }
    else
    {
        return true;
    }
}

bool AwareAgent::generate_limited_domain(int limit)
{

    std::filesystem::create_directories(data_dir);

    std::cout << "Generating limited domain started." << std::endl;

    char command[1000];
    sprintf(command, "python3 %stranslator.py %s %s %s %d >> %s 2>&1 && mv %soutput.sas %s", PULT_PATH, original_domain_path.c_str(), original_problem_path.c_str(), safe_states_filepath.c_str(), limit, log_filepath.c_str(), PULT_PATH, data_dir.c_str());
    int r = std::system(command);

    std::cout << "Generating limited domain ended." << std::endl;

    if (r != 0)
    {
        std::cerr << "AwareAgent could not translate the domain into a limited sas file. Error code " << r << " recieved." << std::endl;
        return false;
    }
    else
    {
        return true;
    }
}

bool AwareAgent::generate_plan()
{

    std::cout << "Generating plan started." << std::endl;

    std::filesystem::create_directories(data_dir);
    DownwardDriver planner(AWARE_AGENT_SEARCH_SETTING, ">> " + log_filepath);

    int limit = unsafeness_limit_start - 1;
    while (!plan_found && limit <= unsafeness_limit_limit)
    {

        limit += 1;

        if (limit == 0)
        {

            int search_result = planner.plan_domain_problem(flat_domain_filepath, flat_problem_filepath, pult_plan_filepath, AWARE_AGENT_SEARCH_TIME_LIMIT);
            if (search_result == 0)
            {
                plan_found = true;
            }
        }
        else
        {

            bool generating_succeeded = generate_limited_domain(limit);
            if (!generating_succeeded)
            {
                break;
            }

            int search_result = planner.plan_sas(data_dir + "output.sas", pult_plan_filepath, AWARE_AGENT_SEARCH_TIME_LIMIT);

            if (search_result == 0)
            {
                plan_found = true;
            }
            else if (search_result < 10 || search_result > 12)
            {
                // return 10, 11, 12 means (in fastdownward planner) unsolvable (with current limit) other results means error
                std::cerr << "AwareAgent could not generate a plan with FastDownward. Error code " << search_result << " recieved." << std::endl;
                break;
            }
        }
    }

    std::cout << "Generating plan ended." << std::endl;
    return plan_found;
}

bool AwareAgent::generate_robust_plan(const SAS_State &state)
{

    auto start = std::chrono::high_resolution_clock::now();

    std::filesystem::create_directories(data_dir);

    auto safe_state = find_next_safe_state(state);

    safe_state.get_problem()->to_strips_file(subproblem_problem_filepath, state.get_problem()->get_init(), safe_state);

    char translate_command[1000];
    sprintf(translate_command, "python3 %stranslator.py %s %s >> %s 2>&1 && mv %sflat_domains/* %s", TFFT_PATH, original_domain_path.c_str(), subproblem_problem_filepath.c_str(), log_filepath.c_str(), TFFT_PATH, data_dir.c_str());
    int r = std::system(translate_command);
    if (r != 0)
    {
        std::cerr << "AwareAgent could not generate a flat domain. Error code " << r << " recieved." << std::endl;
        return false;
    }

    DownwardDriver planner(AWARE_AGENT_SEARCH_SETTING, log_filepath.c_str());
    int search_result = planner.plan_domain_problem(flat_domain_filepath, flat_problem_filepath, partial_robust_plan_filepath, AWARE_AGENT_SEARCH_TIME_LIMIT);

    bool subplan_found = false;

    if (search_result == 0)
    {
        subplan_found = true;
        std::cout << "Robust plan to next safe state found." << std::endl;
    }
    else if (search_result < 10 || search_result > 12)
    {
        // return 12 means (in fastdownward planner) unsolvable (with current limit) other results means error
        std::cerr << "AwareAgent could not generate a plan with FastDownward. Error code " << search_result << " recieved." << std::endl;
    }
    else
    {
        // std::cout << "There is no robust plan to next safe state." << std::endl;
    }

    auto end = std::chrono::high_resolution_clock::now();
    replanning_consumed += std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

    if (subplan_found)
    {
        if (!load_partial_plan())
        {
            return false;
        }
    }

    return true;
}

// implemetation with sets would be better
SAS_State AwareAgent::find_next_safe_state(const SAS_State &state)
{

    /* find the first safe action */
    unsigned int first_safe_action = 0;
    while (is_unsafe(pult_plan[first_safe_action]) && first_safe_action < pult_plan.size())
    {
        first_safe_action += 1;
    }

    /* find corresponding actions */
    size_t n = plan.size();
    std::vector<std::reference_wrapper<const SAS_Action>> actions;
    for (size_t i = 0; i < n; i += 1)
    {
        actions.emplace_back(state.get_problem()->find_action_by_name(plan[i].substr(1, plan[i].size() - 2)));
    }

    /* find subgoal as assignment that actions add and are required by others and goal and are the last */

    size_t j = first_safe_action;
    std::set<size_t> changed_variables;

    // note that minimal effects won't work for domains with conditional effects
    SAS_Assignment required_assignments = actions.at(0).get().get_preconditions();

    for (size_t q = j + 1; q <= n; q += 1)
    {

        for (auto effect : actions.at(q - j - 1).get().get_minimal_effects())
        {
            // note that minimal effects won't work for domains with conditional effects
            changed_variables.emplace(effect.first);
        }

        // note that plain preconditions won't work for domains with conditional effects
        auto required_state = (q < n) ? actions.at(q - j).get().get_preconditions() : state.get_problem()->get_goal().get_assignment();
        for (auto precondition : required_state)
        {
            if (changed_variables.find(precondition.first) == changed_variables.end())
            {
                required_assignments.set(precondition.first, precondition.second);
            }
        }
    }

    return SAS_State(required_assignments, state.get_problem());
}

bool AwareAgent::retranslate_plan()
{

    std::cout << "Plan retranslating started." << std::endl;

    // remove comments at first, then create new file with without retranslated unsafe events
    char command[500];
    sprintf(command, "sed -i -e '/;/d' -e '/(events/d' %s && sed -e 's/-unsafe-copy-[[:digit:]]\\+\\s/ /g' %s > %s", pult_plan_filepath.c_str(), pult_plan_filepath.c_str(), original_plan_filepath.c_str());
    int r = std::system(command);

    std::cout << "Plan retranslating ended." << std::endl;

    if (r != 0)
    {
        std::cerr << "AwareAgent could not retranslate the plan. Error code " << r << " recieved." << std::endl;
        return false;
    }
    else
    {
        return true;
    }
}

bool AwareAgent::load_plan()
{

    std::cout << "Plan loading started." << std::endl;

    plan.clear();
    std::ifstream file(original_plan_filepath);
    if (!file)
    {
        std::cerr << "AwareAgent could not open file '" << original_plan_filepath << "'." << std::endl;
        return false;
    }
    std::string line;
    while (std::getline(file, line))
    {
        line = line.substr(0, line.find_first_of(';'));
        if (line.size() > 0)
        {
            plan.push_back(line);
        }
    }
    file.close();

    pult_plan.clear();
    std::ifstream pult_file(pult_plan_filepath);
    if (!pult_file)
    {
        std::cerr << "AwareAgent could not open file '" << pult_plan_filepath << "'." << std::endl;
        return false;
    }
    std::string pult_line;
    while (std::getline(pult_file, pult_line))
    {
        pult_line = pult_line.substr(0, pult_line.find_first_of(';'));
        if (pult_line.size() > 0)
        {
            pult_plan.push_back(pult_line);
        }
    }
    pult_file.close();

    plan_loaded = true;

    reference_plan_lenght = plan.size();

    std::cout << "Plan loading ended." << std::endl;

    return true;
}

bool AwareAgent::load_partial_plan()
{

    partial_robust_plan.clear();
    std::ifstream file(partial_robust_plan_filepath);
    if (!file)
    {
        std::cerr << "AwareAgent could not open file '" << partial_robust_plan_filepath << "'." << std::endl;
        return false;
    }
    std::string line;
    while (std::getline(file, line))
    {
        line = line.substr(0, line.find_first_of(';'));
        if (line.size() > 0 && line.find("(events") == std::string::npos)
        {
            partial_robust_plan.push_back(line);
        }
    }
    file.close();

    return true;
}

void AwareAgent::find_safe_sequence(const SAS_State &current_state, bool shortest)
{

    auto start = std::chrono::high_resolution_clock::now();
    safe_sequence.clear();
    if (plan_found && plan_loaded)
    {

        std::vector<std::unordered_set<size_t>> p_plus(current_state.get_assignment().size());
        std::vector<std::unordered_set<size_t>> p_minus(current_state.get_assignment().size());
        SAS_State s_i = current_state;
        size_t plan_size = plan.size();

        size_t safe_sequence_lenght = 0;

        /* THIS DOESN'T WORK WITH CONDITIONAL EFFECTS */

        for (size_t i = 0; i < plan_size; i += 1)
        {

            const SAS_Action &a = current_state.get_problem()->find_action_by_name(plan.at(i).substr(1, plan.at(i).size() - 2));

            /* check action applicability */
            if (!s_i.is_applicable(a))
            {
                break;
            }
            else
            {
                bool violation = false;
                for (const auto &p : a.get_preconditions())
                {
                    if (p_minus[p.first].find(p.second) != p_minus[p.first].end())
                    {
                        violation = true;
                        if (shortest)
                        {
                            break;
                        }
                    }
                }
                if (violation)
                {
                    break;
                }
                else
                {
                    if (!is_unsafe(pult_plan.at(i)))
                    {
                        safe_sequence_lenght = i + 1;
                    }
                }
            }

            /* find applicable events */

            auto event_state = p_plus;
            for (const auto &p : s_i.get_assignment())
            {
                event_state[p.first].insert(p.second);
            }
            for (const auto &p : a.get_grounded_effects(s_i))
            {
                event_state[p.first].clear();
                event_state[p.first].insert(p.second);
            }

            std::vector<std::reference_wrapper<const SAS_Event>> e_i;

            for (const SAS_Event &e : current_state.get_problem()->get_events())
            {
                bool contains_all = true;
                for (const auto &p : e.get_preconditions())
                {
                    if (event_state[p.first].find(p.second) == event_state[p.first].end())
                    {
                        contains_all = false;
                        break;
                    }
                }
                if (contains_all)
                {
                    e_i.push_back(e);
                }
            }

            /* change plus_i sets to plus_i+1 */

            for (const auto &p : a.get_grounded_effects(s_i))
            {

                p_plus[p.first].clear();
                p_plus[p.first].insert(p.second);

                p_minus[p.first].clear();
                size_t range_size = current_state.get_problem()->get_variables().at(p.first).get_range();
                for (size_t value = 0; value < range_size; value += 1)
                {
                    if (value != p.second)
                    {
                        p_minus[p.first].insert(value);
                    }
                }
            }

            for (const SAS_Event &e : e_i)
            {
                for (const auto &p : e.get_grounded_effects(s_i))
                {

                    p_plus[p.first].insert(p.second);

                    size_t range_size = current_state.get_problem()->get_variables().at(p.first).get_range();
                    for (size_t value = 0; value < range_size; value += 1)
                    {
                        if (value != p.second)
                        {
                            p_minus[p.first].insert(value);
                        }
                    }
                }
            }

            /*  change state */

            s_i.apply(a);
        }

        for (size_t i = 0; i < safe_sequence_lenght; i += 1)
        {
            safe_sequence.push_back(plan[i]);
        }
    }
    else
    {
        std::cerr << "AwareAgent cannot find safe sequence when plan is not known." << std::endl;
    }

    auto end = std::chrono::high_resolution_clock::now();
    validating_robust_plan_consumed += std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
}

bool AwareAgent::generate_flat_plan()
{
    std::string command = "sed -e 's/(/(events)\\n(/g' " + original_plan_filepath + " | sed 1d > " + flat_plan_filepath;
    int r = std::system(command.c_str());
    if (r != 0)
    {
        std::cerr << "AwareAgent could not generate a flat plan. Error code " << r << " recieved." << std::endl;
        return false;
    }
    else
    {
        return true;
    }
}

void AwareAgent::remove_next_action()
{
    if (!safe_sequence.empty())
    {
        safe_sequence.pop_front();
        plan.pop_front();
        pult_plan.pop_front();
    }
}

bool AwareAgent::is_unsafe(std::string pddl_action)
{
    std::string unsafe_suffix("-unsafe-copy-");
    std::size_t found = pddl_action.find(unsafe_suffix);
    return found != std::string::npos;
}

void AwareAgent::pop_until_next_safe_state()
{
    while (is_unsafe(pult_plan.front()) && !plan.empty())
    {
        plan.pop_front();
        pult_plan.pop_front();
    }
    plan.pop_front();
    pult_plan.pop_front();
}

AwareAgent::~AwareAgent()
{
    if (remove_temp_files)
    {
        std::cout << "Removing temp files." << std::endl;
        if (std::filesystem::remove_all(data_dir) == 0)
        {
            std::cerr << "Temporary files inside " << data_dir << " cannot be deleted." << std::endl;
        }
    }
}

void AwareAgent::show_statistics(unsigned int actions, double overall_time) const
{
    std::cout << "Statistics:" << std::endl;
    std::cout << "number of actions, overall_duration (s), reference plan lenght, number of noops, preprocess consumed (ms), validating consumed (ms), replanning consumed (ms)" << std::endl;
    std::cout << actions << ", " << overall_time << ", " << reference_plan_lenght << ", " << noops_performed << ", " << preprocess_consumed.count() << ", " << validating_robust_plan_consumed.count() << ", " << replanning_consumed.count() << std::endl;
}
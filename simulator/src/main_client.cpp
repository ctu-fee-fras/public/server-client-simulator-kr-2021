#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <streambuf>

#include "client.h"
#include "config.hh"
#include "options.hpp"
#include "exit_codes.h"

#include "SAS_parser.hpp"
#include "SAS_problem.hpp"

int main(int argc, char *argv[])
{

    /* load configuration */

    ClientConfig config;

    try
    {

        config = get_client_config(argc, argv);
    }
    catch (not_enought_arguments &e)
    {

        std::cerr << e.what() << std::endl;
        return CLIENT_NOT_ENOUGH_ARGUMENTS;
    }
    catch (wrong_argument &e)
    {

        std::cerr << e.what() << std::endl;
        return CLIENT_WRONG_ARGUMENT;
    }
    catch (...)
    {

        std::cerr << "Unknown exception caught." << std::endl;
        return CLIENT_UNKNOWN_EXCEPTION;
    }

    /* translate problem to sas */

    // in client we need to translate with events to keep the variables the same
    std::string event_prefix("event-action-prefix-");

    // TODO: consider simplyfing translating and parsing into one call SAS_Parser.parse(domain, problem)

    std::cout << "Translating " << argv[optind] << " and " << argv[optind + 1] << " into output.sas.. ";
    int r = fd_translate_with_events(event_prefix, argv[optind], argv[optind + 1]);
    if (r == 0)
    {
        std::cout << "ok! :)" << std::endl;
    }
    else
    {
        std::cout << "error! :(" << std::endl;
        std::cerr << "Fast Downward translating failed. :(" << std::endl;
        return CLIENT_TRANSLATION_FAILED;
    }

    SAS_Parser sas_parser;
    SAS_Problem sas_problem = sas_parser.parse("./output.sas", event_prefix);

    config.set_sas_problem(&sas_problem);

    int remove_return = system("rm ./output.sas");
    if (remove_return != 0 || WEXITSTATUS(remove_return) != 0)
    {
        std::cerr << "Warning. Removing temporary file './output.sas' failed." << std::endl;
    }

    /* read the problem file into string */

    std::ifstream problem_file(config.get_problem_directory());
    std::string problem_file_text;

    problem_file.seekg(0, std::ios::end);
    problem_file_text.reserve(problem_file.tellg());
    problem_file.seekg(0, std::ios::beg);

    problem_file_text.assign((std::istreambuf_iterator<char>(problem_file)), std::istreambuf_iterator<char>());
    problem_file.close();

    sas_problem.set_strips_text(problem_file_text);

    /* run the client */
    int result;
    result = client(config);

    // see 'exit_codes.h' for more information
    return result;
}
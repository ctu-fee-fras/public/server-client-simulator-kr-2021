#include "agent_selector.hh"

std::shared_ptr<Agent> select_agent(const std::string &name)
{
    if (name == "limit")
    {
        return std::shared_ptr<Agent>(new LimitAgent());
    }
    else if (name == "aware")
    {
        return std::shared_ptr<Agent>(new AwareAgent());
    }
    else if (name == "app")
    {
        return std::shared_ptr<Agent>(new APPAgent());
    }
    else if (name == "fond")
    {
        return std::shared_ptr<Agent>(new FONDAgent());
    }
    else
    {
        std::cerr << "Invalid agent name used. Using APPAgent instead." << std::endl;
        return std::shared_ptr<Agent>(new APPAgent());
    }
}
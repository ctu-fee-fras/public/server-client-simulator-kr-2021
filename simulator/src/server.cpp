#include "server.h"

// accepts connections and launches the main problem
int server(ServerConfig &config)
{

    // Server socket
    struct sockaddr_in addr;
    int server_socket;

    // Client socket
    int client_socket;
    socklen_t addrlength;

    unsigned int port = config.get_port();

    if ((server_socket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        std::cerr << "Server could not create socket." << std::endl;
        return -5;
    }

    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(port);
    addr.sin_family = AF_INET;

    if (bind(server_socket, (sockaddr *)&addr, sizeof(addr)))
    {
        std::cerr << "Server could not bind socket." << std::endl;
        return -5;
    }

    if (listen(server_socket, 1))
    {
        std::cerr << "Server listening failed." << std::endl;
        return -6;
    }

    std::cout << "Server is running on port " << port << "." << std::endl;
    addrlength = sizeof(addr);

    while ((client_socket = accept(server_socket, (sockaddr *)&addr, &addrlength)) >= 0)
    {

        // argument of the host_problem containing the client_socket and the configuration set in main
        std::pair<int, const ServerConfig *> *p = new std::pair<int, const ServerConfig *>();
        p->first = client_socket;
        p->second = &config;

        pthread_attr_t t_attr;
        pthread_t t_id;
        pthread_attr_init(&t_attr);

        // call the main function host_problem
        if (!pthread_create(&t_id, &t_attr, host_problem, p))
        {
            pthread_detach(t_id);
        }
    }

    close(server_socket);
    return 0;
}

void *host_problem(void *arg)
{

    unsigned int *r = (unsigned int *)malloc(sizeof(unsigned int));

    auto start_time = std::chrono::steady_clock::now();
    srand(time(NULL));

    std::pair<int, ServerConfig *> *p = (std::pair<int, ServerConfig *> *)arg;
    int client_socket = p->first;
    ServerConfig *conf = p->second;
    delete p;
    unsigned int turn_limit = conf->get_turn_lim();
    float time_limit = conf->get_time_lim();

    std::cout << std::endl
              << "Initializing a new problem." << std::endl;

    // initializing current state to init state
    const SAS_Problem *problem = conf->get_sas_problem();
    SAS_State current_state = problem->get_init();
    SAS_State goal = problem->get_goal();

    bool goal_achieved = false;
    unsigned int turn = 0;
    auto current_time = std::chrono::steady_clock::now();

    std::chrono::duration<double> elapsed_time;
    std::ostringstream os;
    goal_achieved = current_state.is_goal();

    int type_error = 0;

    while (!goal_achieved)
    {

        current_time = std::chrono::steady_clock::now();
        elapsed_time = current_time - start_time;

        turn += 1;

        if (turn > turn_limit)
        {
            type_error = 1;
            break;
        }

        if (elapsed_time.count() > time_limit)
        {
            type_error = 2;
            break;
        }

        try
        {
            send_state_to_client(client_socket, current_state);
        }
        catch (communication_failed &e)
        {
            std::cerr << e.what() << std::endl;
            *r = SERVER_COMMUNICATION_FAILED;
            return r;
        }

        std::string action_string;
        try
        {
            action_string = recieve_action(client_socket);
        }
        catch (client_surrender &e)
        {
            std::cout << "Client surrendered." << std::endl;
            *r = SERVER_SURRENDER;
            return r;
        }
        catch (bad_action &e)
        {
            std::cerr << e.what() << std::endl;
            type_error = 4;
            break;
        }

        try
        {

            const SAS_Action &action = problem->find_action_by_name(action_string);

            if (current_state.is_applicable(action))
            {
                current_state.apply(action);
            }
            else
            {
                std::cerr << "Client handling thread encountered unapplicable action." << std::endl;
                type_error = 3;
                break;
            }
        }
        catch (SAS_not_found &not_found)
        {

            std::cerr << not_found.what() << std::endl;
            std::cerr << "Client handling thread encountered unknown action." << std::endl;
            type_error = 4;
            break;
        }

        auto events_to_apply = generate_events(current_state, problem);
        if (events_to_apply.size() != 0)
        {
            std::cout << "turn " << turn << ":" << std::endl;
            for (auto &event : events_to_apply)
            {
                std::cout << "  " << event.get().get_name() << std::endl;
                if (!current_state.check_and_apply(event))
                {
                    std::cerr << "Action application failed." << std::endl;
                }
            }
        }

        // TODO: when to check this
        goal_achieved = current_state.is_goal();
    }

    auto end_time = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed_seconds = end_time - start_time;
    std::cout << "time: " << elapsed_seconds.count() << std::endl;

    std::string message = get_final_message(goal_achieved, type_error, turn, elapsed_seconds.count());

    try
    {
        send_final_message(client_socket, message);
    }
    catch (communication_failed &e)
    {
        std::cerr << e.what() << std::endl;
        *r = SERVER_COMMUNICATION_FAILED;
        return r;
    }

    return r;
}

void send_state_to_client(const int client_socket, SAS_State &current_state)
{

    const unsigned int number_of_variables = current_state.get_assignment().size();

    std::vector<size_t> variables(number_of_variables, 0);

    for (size_t i = 0; i < number_of_variables; i += 1)
    {
        variables[i] = current_state.get(i);
    }

    unsigned int message_size = 7 + sizeof(size_t) * number_of_variables + 8;

    if (!write(client_socket, &message_size, sizeof(unsigned int)))
    {
        const std::string error("Client handling thread encountered error while sending state lenght.");
        std::cerr << error << std::endl;
        throw communication_failed(error);
    }

    std::vector<char> message(message_size);
    strcpy(message.data(), "<state>");
    memcpy(message.data() + 7, variables.data(), sizeof(size_t) * number_of_variables);
    strcpy(message.data() + message_size - 8, "</state>");

    if (!write(client_socket, message.data(), message_size))
    {
        const std::string error("Client handling thread encountered error while sending state.");
        std::cerr << error << std::endl;
        throw communication_failed(error);
    }
}

const std::string recieve_action(const int socket)
{

    unsigned int message_lenght;
    if (!read(socket, &message_lenght, sizeof(unsigned int)))
    {
        const std::string error("Client handling thread failed while recieving client response about action lenght.");
        std::cerr << error << std::endl;
        throw communication_failed(error);
    }

    std::vector<char> buffer(message_lenght);
    if (!read(socket, buffer.data(), message_lenght))
    {
        const std::string error("Client handling thread failed while recieving client action response.");
        std::cerr << error << std::endl;
        throw communication_failed(error);
    }

    std::string message(buffer.begin(), buffer.end());

    if (is_message_action(message))
    {
        return message.substr(8, message.size() - 8 - 9);
    }
    else if (is_message_surrender(message))
    {
        throw client_surrender();
    }
    else
    {
        const std::string error("Bad action message recieved.");
        throw bad_action(error);
    }
}

inline bool is_message_surrender(const std::string &message)
{
    return message == MESSAGE_SURRENDER;
}

inline bool is_message_action(const std::string &message)
{
    return message.size() >= 17 && message.compare(0, 8, "<action>") == 0 && message.compare(message.size() - 9, 9, "</action>") == 0;
}

std::vector<std::reference_wrapper<const SAS_Event>> generate_events(const SAS_State &current_state, const SAS_Problem *problem)
{

    std::vector<std::reference_wrapper<const SAS_Event>> applicable_events = problem->get_applicable_events(current_state);

    size_t number_of_selectable_events = applicable_events.size();

    std::vector<long int> required(problem->get_number_of_variables(), -1);
    std::vector<long int> changed(problem->get_number_of_variables(), -1);
    std::vector<bool> selectable(applicable_events.size(), true);
    std::vector<std::reference_wrapper<const SAS_Event>> selected_events;

    // select number from 0 to number_of_selectable_events
    // this number is position of nth action from selectable applicable_events
    // 0 stands for noop event and it is used for termination
    size_t event_order;
    while ((event_order = rand() % (number_of_selectable_events + 1)) != 0)
    { // noop stand for 0

        /* find real index into applicable_events */
        size_t effect_index = 0;
        size_t current_order = 0;
        while (current_order != event_order)
        {
            while (!selectable[effect_index])
            {
                effect_index += 1;
            }
            current_order += 1;
            effect_index += 1;
        }
        effect_index -= 1;

        /* 
         * check if concurrently applicable 
         * ---------------------------------
         * 
         * event e_1 is concurrently applicable with set of events e_s 
         *      iff 
         * 1) for all events e from e_s: doesn't exist v such that: eff(e)[v] != pre(e_1)[v], for each v in vars(eff(e)) \cap vars(pre(e_1))
         *              - (e_1 will still be applicable no matter what ordering we choose)
         *              - (if event e_1 and e share a variable effect change it to the same value as precondition requires or e doesn't change it at all)
         *      and
         * 2) for all events e from e_s: doesn't exist v such that: pre(e)[v] != eff(e_1)[v], for each v in vars(pre(e)) \cap vars(eff(e_1))
         *              - (e_1 doesn't change a variables required by another events - e will still be applicable no matter what ordering we choose)
         *              - (if event e_1 and e share a variable effect change it to the same value as precondition requires or e doesn't change it at all)
         *      and
         * 3) for all events e from e_s: doesn't exist v such that: pre(e)[v] != pre(e_1)[v], for each v in vars(pre(e)) \cap vars(pre(e_1))
         *              - (if e_1 has the same affected variable in the preconditions, they must have the same required value)
         *      and
         * 4) for all events e from e_s: doesn't exist v such that: eff(e)[v] != eff(e_1)[v], for each v in vars(eff(e)) \cap vars(eff(e_1))
         *              - (if e_1 has the same affected variable in the effects, they must have the same effect value)
         */

        bool concurrently_applicable = true;

        // effects & preconditions
        SAS_Assignment grounded_preconditions = applicable_events[effect_index].get().get_grounded_preconditions(current_state);
        SAS_Assignment grounded_effects = applicable_events[effect_index].get().get_grounded_effects(current_state);

        // 1) check if preconditions are the same
        if (concurrently_applicable)
        {
            for (auto precondition : grounded_preconditions)
            {
                // TODO. fix integer cast - this may overflow
                if (changed[precondition.first] != -1 && changed[precondition.first] != (long int)precondition.second)
                {
                    concurrently_applicable = false;
                    break;
                }
            }
        }

        // 2) check if effects don't change required preconditions
        if (concurrently_applicable)
        {
            for (auto effect : grounded_effects)
            {
                // TODO. fix integer cast - this may overflow
                if (required[effect.first] != -1 && required[effect.first] != (long int)effect.second)
                {
                    concurrently_applicable = false;
                    break;
                }
            }
        }

        // 3) is checked by considering only applicable events

        // 4) check if effects don't override another effects (if both effects are the same, it's not a violation)
        if (concurrently_applicable)
        {
            for (auto effect : grounded_effects)
            {
                // TODO. fix integer cast - this may overflow
                if (changed[effect.first] != -1 && changed[effect.first] != (long int)effect.second)
                {
                    concurrently_applicable = false;
                    break;
                }
            }
        }

        /* if concurrently applicable, add it */
        if (concurrently_applicable)
        {
            for (auto precondition : grounded_preconditions)
            {
                required[precondition.first] = precondition.second;
            }
            for (auto effect : grounded_effects)
            {
                changed[effect.first] = effect.second;
            }
            selected_events.emplace_back(applicable_events[effect_index]);
            selectable[effect_index] = false; // if selected, don't select it again
            number_of_selectable_events -= 1;
        }
        else
        {
            selectable[effect_index] = false; // if not concurrently applicable, don't try to select it again
            number_of_selectable_events -= 1;
        }
    }

    return selected_events;
}

std::string get_final_message(const bool goal_achieved, const int type_error, const int turn, const double elapsed_seconds)
{

    std::string message;

    if (type_error == 0 && goal_achieved)
    {

        std::cout << "Goal achieved by client." << std::endl;
        message = "<win>";
        message += "<turn>" + std::to_string(turn) + "</turn>";
        message += "<time>" + std::to_string(elapsed_seconds) + "</time>";
        message += "</win>";
    }
    else if (type_error == 1)
    {

        std::cout << "Client failed. He is out of turns." << std::endl;
        message = MESSAGE_OUT_OF_TURNS;
    }
    else if (type_error == 2)
    {

        std::cout << "Client failed. He is out of time." << std::endl;
        message = MESSAGE_OUT_OF_TIME;
    }
    else if (type_error == 3)
    {

        std::cout << "Client sent a nonapplicable action." << std::endl;
        message = MESSAGE_NONAPPLICABLE_ACTION;
    }
    else if (type_error == 4)
    {

        std::cout << "Client sent a bad action." << std::endl;
        message = MESSAGE_BAD_ACTION;
    }

    return message;
}

void send_final_message(const int client_socket, const std::string &message)
{

    unsigned int lenght = message.length();
    if (!write(client_socket, &lenght, sizeof(unsigned int)))
    {
        std::string error = "Client handling thread failed while sending result length.";
        throw communication_failed(error);
    }

    if (!write(client_socket, message.c_str(), lenght))
    {
        std::string error = "Client handling thread failed while sending result.";
        throw communication_failed(error);
    }
}

# Server-Client-like Planning Environment

## Building

### Which OS
This tool has not been tested in any Windows environment. The program was developed in Ubuntu-based OS, so it should work on any Linux OS flawlessly. Windows environments may encounter some path errors, and since both `server` and `client` are using UNIX programs occasionally (through `bash`), it may cause a problem.

### Dependencies
To correctly run all parts of environment, you need several packages to properly build and run this programs. There is list of required packages:
* `g++` (version at least 5.2 - standard `gnu++17` is required)
* `make`
* `python3` - python is causing many problems with version; alliases python3->python3.7 and python->python3.7 are recommended
* `sed` (used by `LimitAgent`, `AwareAgent`, `APPAgent`)

### Building
`make clean && make`

## Running
Currently, four agents are implemented. Therefore it is necessary to specify which one to use. It is done with `[-a|--agent] <agent-id>`. After selection, it is necessary to specify all optional parameters needed by the agent. By default or with the wrong `agent-id`,  `APPAgent` will be selected. 

Binaries are supposed to be run in the `./simulator` folder. They (still) use relative paths. Nobody fixed that yet.

### Server
`./server <domain_filepath> <problem_filepath>`

### Client
`./client [-a|--agent <agent-id>] [-S|--safe-states <safe-states-path>] <domain_filepath> <problem_filepath>` 

## Exit codes

### Server
* 0 - no problems occurred
* 255 - invalid arguments
* 254 - temporary copy without comments cannot be parsed properly
* 253 - temporary copy without comments of one of the input files cannot be created
* 252 - checking of domain and problem failed; there is something wrong
* 251 - creating or binding socket failed
* 250 - socket listening failed

### Client
* 0 - no problems occurred
* 255 - invalid arguments
* 254 - temporary copy without comments cannot be parsed properly
* 253 - temporary copy without comments of one of the input files cannot be created
* 252 - checking of domain and problem failed; there is something wrong
* 251 - client failed while running Fast Downward translator
* 250 - client connecting to server failed
* 249 - client writing to or reading from socket failed
* 248 - method Agent::act() returned invalid action
* 247 - client received corrupted state
* 246 - agent ran out of turns
* 245 - agent ran out of time
* 244 - agent surrendered 
* 243 - client sent an invalid action
* 242 - client sent a nonapplicable action

## Agents

### Agent identifiers
In format `class_name - id`.
* `LimitAgent` - `limit`
* `AwareAgent` - `aware`
* `APPAgent` - `app`
* `FONDAgent` - `fond`

### Agent requirements
Each agent may require a different set of information. Each time you select one of these agents, you need to supply the client with the required information. Otherwise, the client will most probably fail.

Agents are using python tools to reason. Therefore it is required to install them. Required packages are: `mypy`.

#### **Limit Agent (limit)**
This agent requires (to properly reason) `FastDownward` planner, `toFlatFond` translator, `unsafenessLimit` translator. Each of these programs has to have a specified path to binary in the `./simulator/include/paths.h` file. 
It also requires `bash` and one UNIX program: `sed`.

When running `client` binary with selected `LimitAgent` you need to specify `-S <filepath>` or its longer alias `--safe-states <filepath>`.

Example: `./client -a limit -S ../domains/perestroika/safe1.txt ../domains/perestroika/domain.pddl ../domains/perestroika/problem1.pddl`

#### **Aware Agent (aware)**
Requirements are the same as `LimitAgent` has.

Example: `./client -a aware -S ../domains/perestroika/safe1.txt ../domains/perestroika/domain.pddl ../domains/perestroika/problem1.pddl`

#### **App Agent (app)**
This agent requires to properly reason `FastDownward` planner, `toFlatFond` translator, `safeStatesFinderTranslator` translator. Each of these programs has to have a specified path to binary in the `./simulator/include/paths.h` file. 
It also requires `bash` and one UNIX program: `sed`.

Example: `./client -a app ../domains/perestroika/domain.pddl ../domains/perestroika/problem1.pddl`

#### **FOND Agent (fond)**
This agent requires to properly reason the [`PRP` planner](https://github.com/QuMuLab/planner-for-relevant-policies/tree/master) and the `toFOND` translator. Each of these programs has to have a specified path to binary in the `./simulator/include/paths.h` file. 

Example: `./client -a fond ../domains/perestroika/domain.pddl ../domains/perestroika/problem.pddl`

## Agent setting
Some agents have customizable values in `simulator/include/agents_setting.h`. They may be changed for achieving slightly different behaviors. Remember to build binaries again. (Command `make` may overlook changes in `.h` file.)

## Python version issues

Python scripts are called from the `C++` code (`simlulator/src/`). Command lines are (at the moment) hardcoded. It is known that for the App Agent (`app_agent.cpp, Line 116`), running the script for determining suitability of the domain and reversible and irreversible events (Alg. 1 and 2 in the paper) requires `Python` version `3.7` or above. It might be necessary to amend command lines requiring a specific `Python` version.

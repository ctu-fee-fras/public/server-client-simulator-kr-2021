#ifndef __AGENT_SELECTOR__
#define __AGENT_SELECTOR__

#include <string>
#include <memory>

#include "agent.hh"
#include "limit_agent.hh"
#include "aware_agent.hh"
#include "app_agent.hh"
#include "fond_agent.hh"

std::shared_ptr<Agent> select_agent(const std::string& name);

#endif
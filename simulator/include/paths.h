#pragma once

#define PULT_PATH "../pddl-unsafeness-limit-translator/"
#define TFFT_PATH "../toflatfond/"
#define FD_PATH "../downward/"
#define VAL_PATH "../validator/"
#define REVT_PATH "../pddl-safe-states-finder/"
#define TOFOND_PATH "../toFOND/"
#define PRP_PATH "../PRP/"

#pragma once

#define LIMIT_AGENT_MAX_UNSAFE_LIMIT 1000
#define LIMIT_AGENT_STARTING_UNSAFE_LIMIT 0
// --search is not working.. placeholder string needs improvement, now its working only for --alias and --portfolio
#define LIMIT_AGENT_SEARCH_SETTING "--alias \"lama-first\""
#define LIMIT_AGENT_SEARCH_TIME_LIMIT "1m"
#define LIMIT_AGENT_DELETE_TEMP_FILES true

#define AWARE_AGENT_MAX_UNSAFE_LIMIT 1000
#define AWARE_AGENT_STARTING_UNSAFE_LIMIT 0
// --search is not working.. placeholder string needs improvement, now its working only for --alias and --portfolio
#define AWARE_AGENT_SEARCH_SETTING "--alias \"lama-first\""
#define AWARE_AGENT_SEARCH_TIME_LIMIT "1m"
#define AWARE_AGENT_DELETE_TEMP_FILES true

// --search is not working.. placeholder string needs improvement, now its working only for --alias and --portfolio
#define APP_AGENT_SEARCH_SETTING "--alias \"lama-first\""
#define APP_AGENT_SEARCH_TIME_LIMIT "1m"
#define APP_AGENT_DELETE_TEMP_FILES true

#define FOND_AGENT_SEARCH_MEMORY_LIMIT 4000000000   // B
#define FOND_AGENT_SEARCH_CPU_TIME_LIMIT 60         // seconds
#define FOND_AGENT_DELETE_TEMP_FILES true
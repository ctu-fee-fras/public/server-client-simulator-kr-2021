#pragma once

#include <cstdlib>
#include <getopt.h>
#include <iostream>

#include "config.hh"
#include "exceptions.hpp"

ClientConfig get_client_config(int argc, char* argv[]);
void client_usage(char *filename);

ServerConfig get_server_config(int argc, char* argv[]);
void server_usage(char *filename);
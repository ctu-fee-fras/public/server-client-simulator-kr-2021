#ifndef __AGENT_HH__
#define __AGENT_HH__

#include <iostream>

#include "SAS_state.hpp"

class LimitAgent;
class AwareAgent;
class APPAgent;

/*
 *
 *  Client's brain. Used for acting and decision making.
 *
 */
class Agent {
  public:

    Agent() {};
    virtual ~Agent() {};

    /*
     *
     * Return a next agent decision. The decision may be acquired by planning,
     * randomness or whatever.
     * Return: action (std::string) in format used by
     * FastDownward planner, "" stands for errror
     *
     */
    virtual std::string act(const SAS_State& current_state, std::string domain_filepath, std::string problem_filepath, std::string safe_states_filepath) = 0;
    
    virtual void show_statistics(unsigned int actions, double overall_duration) const;
};

#endif //__AGENT_HH__

#ifndef _SERVER_H_
#define _SERVER_H_

#include <stdlib.h>
#include <sys/socket.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

#include <experimental/filesystem>
#include <cstring>
#include <iostream>
#include <string>
#include <tuple>
#include <chrono>
#include <bitset>
#include <sstream>
#include <fstream>

#include "SAS_problem.hpp"
#include "SAS_exceptions.hpp"

#include "utility.h"
#include "config.hh"
#include "exit_codes.h"

#include "exceptions.hpp"

#include "messages.h"

/*
 *
 * The main functions for the server
 *
 */

int server(ServerConfig &config);
void *host_problem(void *arg);

void send_state_to_client(const int socket, SAS_State &current_state);
const std::string recieve_action(const int socket);

std::vector<std::reference_wrapper<const SAS_Event>> generate_events(const SAS_State &current_state, const SAS_Problem *problem);
inline bool is_message_action(const std::string &message);
inline bool is_message_surrender(const std::string &message);

std::string get_final_message(const bool goal_achieved, const int type_error, const int turn, const double elapsed_seconds);
void send_final_message(const int client_socket, const std::string &message);

#endif //_SERVER_H_

#ifndef _CLIENT_H_
#define _CLIENT_H_

#include <filesystem>
#include <fstream>
#include <sys/socket.h>
#include <pthread.h>
#include <netinet/in.h>

#include <arpa/inet.h>
#include <cstring>
#include <netinet/tcp.h>
#include <sys/types.h>
#include <unistd.h>
#include <sstream>
#include <netdb.h>

#include <iostream>
#include <string>
#include <tuple>
#include <memory>

#include "config.hh"
#include "utility.h"
#include "agent.hh"
#include "limit_agent.hh"
#include "aware_agent.hh"
#include "app_agent.hh"
#include "agent_selector.hh"
#include "exit_codes.h"
#include "messages.h"
#include "exceptions.hpp"

#include "SAS_assignment.hpp"

int client(ClientConfig& conf); // the main function: connects to the server (using the connect function), receives the state from the server and choses the action to send
int connect(const char *hostname, unsigned int port); // connects to the server, using it's adress and the port

unsigned int recieve_state(int socket, SAS_State& current_state, bool& run, const std::shared_ptr<const Agent>& agent);
bool message_is_state(const std::string& message);
bool message_is_win(const std::string& message);
void process_state_message(const std::string& message, SAS_State& current_state);
unsigned int process_win_message(const std::string& message, const std::shared_ptr<const Agent>& agent);
int send_action(const int socket, const std::string& action);
void send_surrender(const int socket);

#endif //_CLIENT_H

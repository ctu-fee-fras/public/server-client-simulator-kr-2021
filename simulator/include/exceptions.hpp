#ifndef ___SIMULATOR_EXCEPTIONS___
#define ___SIMULATOR_EXCEPTIONS___

#include <exception>

class communication_failed : public std::exception
{

private:
    std::string what_message;

public:
    communication_failed(const std::string message) : what_message(message){};

    virtual const char *what() noexcept
    {
        return what_message.c_str();
    }
};

class bad_action : public std::exception
{

private:
    std::string what_message;

public:
    bad_action(const std::string message) : what_message(message){};

    virtual const char *what() noexcept
    {
        return what_message.c_str();
    }
};

class wrong_argument : public std::exception
{

private:
    std::string what_message;

public:
    wrong_argument(const std::string message) : what_message(message){};

    virtual const char *what() noexcept
    {
        return what_message.c_str();
    }
};

class not_enought_arguments : public std::exception
{

private:
    std::string what_message;

public:
    not_enought_arguments(const std::string message) : what_message(message){};

    virtual const char *what() noexcept
    {
        return what_message.c_str();
    }
};

class client_surrender : public std::exception
{
};

#endif
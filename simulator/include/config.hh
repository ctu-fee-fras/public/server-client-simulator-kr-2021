#pragma once

#include <memory>

#include "SAS_problem.hpp"

#define __CONFIG_DEFAULT_MAX_TURN 1000                // the max turn set by default
#define __CONFIG_DEFAULT_MAX_TIME 1000                // the max time set by default
#define __CONFIG_DEFAULT_PORT 2222                    // the default port for the server
#define __CONFIG_CLIENT_DEFAULT_PORT 2222             // the default port the client will try to connect to
#define __CONFIG_CLIENT_DEFAULT_SERVER_IP "localhost" // the default IP of the server
#define __CONFIG_DEFAULT_AGENT "app"

/* contains basic parameters set by the user for both the server and the client */

// these classes are only used to save parameters and make them usable by the server/client
class ServerConfig
{

public:
    //Constructor
    ServerConfig();

    //getters
    unsigned int get_turn_lim() const;
    float get_time_lim() const;
    unsigned int get_port() const;
    const SAS_Problem *get_sas_problem() const;

    //setters
    void set_sas_problem(SAS_Problem *problem);
    void set_turn_lim(unsigned int turn_limit);
    void set_time_lim(float time_lim);
    void set_port(unsigned int port);

private:
    SAS_Problem *_problem;    // parsed problem for server
    unsigned int _turn_limit; // the number of turns acceptable before the server stops the simulation (the client loses)
    float _time_limit;        // the number of time acceptable before the server stops the simulation (the client lost)
    unsigned int _port;       // the port used by the server
};

class ClientConfig
{

public:
    ClientConfig();

    const SAS_Problem *get_sas_problem() const;
    unsigned int get_port() const;
    std::string get_server_ip() const;
    std::string get_domain_directory() const;
    std::string get_problem_directory() const;
    std::string get_planner() const;
    std::string get_dead_ends() const;
    std::string get_safe_states() const;
    unsigned int get_max_distance() const;
    unsigned int get_danger_value() const;
    bool get_set_cost() const;
    std::string get_agent_id() const;

    void set_port(unsigned int port);
    void set_server_ip(const std::string &server_ip);
    void set_domain_directory(const std::string dd);
    void set_problem_directory(const std::string pd);
    void set_planner(const std::string planner);
    void set_dead_ends(const std::string dead_ends);
    void set_safe_states(const std::string safe_states_filepath);
    void set_max_distance(const unsigned int max_distance);
    void set_danger_value(const unsigned int danger_value);
    void set_sas_problem(SAS_Problem *problem);
    void set_set_cost(bool b);
    void set_agent_id(std::string id);

private:
    SAS_Problem *_problem;
    unsigned int _port;             // the port it will try to connect to
    std::string _serverip;          // the host it will try to connect to
    std::string _domain_directory;  // saves where the domain is for further use
    std::string _problem_directory; // same for the problem (unused for now)
    std::string _planner;           // saves the link to the planner
    std::string _safe_states;       // path to safe-states file
    bool _set_cost;
    std::string _agent_id;
};

#ifndef __AWARE_AGENT_HH__
#define __AWARE_AGENT_HH__

#include <functional>
#include <filesystem>
#include <sstream>
#include <string>
#include <deque>
#include <fstream>
#include <algorithm>
#include <chrono>
#include <set>
#include <ctime>

#include "client.h"
#include "paths.h"
#include "agent.hh"
#include "agents_setting.h"
#include "downward_driver.hh"

class AwareAgent : public Agent {

  public:
    AwareAgent();
    ~AwareAgent();
    std::string act(const SAS_State& current_state, std::string domain_filepath, std::string problem_filepath, std::string safe_states_filepath);
    void show_statistics(unsigned int actions, double overall_actions) const override;

  private:

    std::time_t timestamp_id;
    std::string data_dir;
    std::string original_domain_path;
    std::string original_problem_path;
    std::string safe_states_filepath;
    std::string flat_domain_filepath;
    std::string flat_problem_filepath;
    std::string subproblem_problem_filepath;
    std::string partial_problem_filepath;
    std::string flat_plan_filepath;
    std::string pult_plan_filepath;
    std::string original_plan_filepath;
    std::string flat_subplan_filepath;
    std::string partial_robust_plan_filepath;
    std::string validation_result_filepath;
    std::string log_filepath;

    unsigned int reference_plan_lenght = 0;
    unsigned int noops_performed = 0;

    std::chrono::duration<long, std::milli> preprocess_consumed;
    std::chrono::duration<long, std::milli> validating_robust_plan_consumed;
    std::chrono::duration<long, std::milli> replanning_consumed;

    const int unsafeness_limit_start = AWARE_AGENT_STARTING_UNSAFE_LIMIT;
    const int unsafeness_limit_limit = AWARE_AGENT_MAX_UNSAFE_LIMIT;
    bool remove_temp_files = AWARE_AGENT_DELETE_TEMP_FILES;

    bool init_done = false;
    bool plan_found = false;
    bool plan_loaded = false;
    std::deque<std::string> plan;
    std::deque<std::string> pult_plan;
    std::deque<std::string> safe_sequence;
    std::deque<std::string> partial_robust_plan;

    void init(const SAS_State& current_state, std::string domain_filepath, std::string problem_filepath, std::string safe_states_filepath);
    bool generate_flat_domain_and_problem(const SAS_State& current_state);
    bool generate_limited_domain(int limit);
    bool generate_plan();
    bool generate_robust_plan(const SAS_State& current_state);
    bool retranslate_plan();
    bool load_plan();
    void find_safe_sequence(const SAS_State& state, bool shortest = false);
    bool generate_flat_plan();
    void remove_next_action();
    bool is_unsafe(std::string pddl_action);
    void pop_until_next_safe_state();
    SAS_State find_next_safe_state(const SAS_State& current_state);
    bool load_partial_plan();

};

#endif //__AWARE_AGENT_HH__
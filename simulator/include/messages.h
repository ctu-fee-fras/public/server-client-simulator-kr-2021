#pragma once

#define MESSAGE_OUT_OF_TURNS "<lost><outofturn></lost>"
#define MESSAGE_OUT_OF_TIME "<lost><outoftime></lost>"
#define MESSAGE_NONAPPLICABLE_ACTION "<broken><nonapplicableaction></broken>"
#define MESSAGE_BAD_ACTION "<broken><badaction></broken>"

#define MESSAGE_SURRENDER "<surrender>"

#ifndef __FOND_AGENT_HH__
#define __FOND_AGENT_HH__

#include <filesystem>
#include <string>
#include <fstream>
#include <regex>
#include <ctime>

#include "client.h"
#include "paths.h"
#include "agent.hh"
#include "agents_setting.h"

#include "SAS_problem.hpp"
#include "SAS_parser.hpp"
#include "SAS_variable_info.hpp"

class FONDAgent : public Agent
{

public:
  FONDAgent();
  ~FONDAgent();
  std::string act(const SAS_State &current_state, std::string domain_filepath, std::string problem_filepath, std::string safe_states_filepath);
  void show_statistics(unsigned int actions, double overall_actions) const override;

private:
  bool init_done;

  std::chrono::duration<long, std::milli> preprocess_consumed;
  unsigned int number_of_noops = 0;

  std::time_t timestamp_id;
  std::string data_dir;
  std::string fond_domain;
  std::string fond_problem_filepath;
  std::string policy_filepath;
  std::string translated_policy_filepath;
  std::string log_filepath;

  bool remove_temp_files;

  std::shared_ptr<const SAS_Problem> fond_problem;

  void init(std::string domain_filepath, std::string problem_filepath);
  bool translate_to_FOND(std::string domain_filepath, std::string problem_filepath);
  bool find_policy();
  std::string find_action(const SAS_State &state);
  bool load_fond_problem();
  std::string find_corresponding_action(const std::string &variable_name, int value);
  bool translate_strategy();
  std::string negate_strips(const std::string &strips_action);
  bool contains_strips(const std::string &strips_action, const std::string &string);
};

#endif //__FOND_AGENT_HH__
#ifndef __LIMIT_AGENT_HPP__
#define __LIMIT_AGENT_HPP__

#include <filesystem>
#include <sstream>
#include <string>
#include <deque>
#include <fstream>
#include <algorithm>
#include <chrono>
#include <unistd.h>
#include <utility>
#include <ctime>
#include <memory>
#include <vector>
#include <unordered_set>

#include "client.h"
#include "paths.h"
#include "agent.hh"
#include "agents_setting.h"

#include "SAS_exceptions.hpp"
#include "SAS_problem.hpp"
#include "SAS_state.hpp"

class LimitAgent : public Agent
{

public:
  LimitAgent();
  ~LimitAgent();
  std::string act(const SAS_State &current_state, std::string domain_filepath, std::string problem_filepath, std::string safe_states_filepath);
  void show_statistics(unsigned int actions, double overall_actions) const override;

private:
  std::time_t timestamp_id;
  std::string data_dir;
  std::string original_domain_path;
  std::string original_problem_path;
  std::string safe_states_filepath;
  std::string flat_domain_filepath;
  std::string flat_problem_filepath;
  std::string flat_sas_problem_filepath;
  std::string partial_problem_filepath;
  std::string flat_plan_filepath;
  std::string pult_plan_filepath;
  std::string original_plan_filepath;
  std::string flat_subplan_filepath;
  std::string validation_result_filepath;
  std::string log_filepath;

  unsigned int reference_plan_lenght = 0;
  unsigned int noops_performed = 0;

  std::chrono::duration<long, std::milli> preprocess_consumed;
  std::chrono::duration<long, std::milli> validating_robust_plan_consumed;

  const int unsafeness_limit_start = LIMIT_AGENT_STARTING_UNSAFE_LIMIT;
  const int unsafeness_limit_limit = LIMIT_AGENT_MAX_UNSAFE_LIMIT;
  bool remove_temp_files = LIMIT_AGENT_DELETE_TEMP_FILES;

  bool init_done = false;
  bool plan_found = false;
  bool plan_loaded = false;
  std::deque<std::string> plan;
  std::deque<std::string> pult_plan;
  std::deque<std::string> safe_sequence;

  std::shared_ptr<SAS_Problem> flat_sas_problem;

  void init(const SAS_State &state, std::string domain_filepath, std::string problem_filepath, std::string safe_states_filepath);
  bool generate_flat_domain_and_problem(const SAS_State &state);
  bool generate_limited_domain(int limit);
  bool generate_plan();
  bool retranslate_plan();
  bool load_plan();
  bool load_flat_sas_problem();
  void find_safe_sequence(const SAS_State &state, bool shortest = false);
  bool generate_flat_plan();
  void remove_next_action();
  bool is_unsafe(std::string pddl_action);
};

#endif //__LIMIT_AGENT_HH__
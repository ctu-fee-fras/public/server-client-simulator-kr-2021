#include "SAS_operator.hpp"

const SAS_Assignment SAS_Operator::get_grounded_preconditions(const SAS_State &state) const
{
    SAS_Assignment grounded_preconditions(_preconditions);
    for (auto conditional_effect : _effects)
    {
        if (state.satisfy_all(conditional_effect.first))
        {
            for (auto precondition : conditional_effect.first)
            {
                grounded_preconditions.set(precondition.first, precondition.second);
            }
        }
    }
    return grounded_preconditions;
}

const SAS_Assignment SAS_Operator::get_grounded_effects(const SAS_State &state) const
{
    SAS_Assignment grounded_effects;
    for (auto conditional_effect : _effects)
    {
        if (state.satisfy_all(conditional_effect.first))
        {
            grounded_effects.apply(conditional_effect.second);
        }
    }
    return grounded_effects;
}

const SAS_Assignment SAS_Operator::get_minimal_effects() const
{
    SAS_Assignment minimal_effects;
    for (auto conditional_effect : _effects)
    {
        if (conditional_effect.first.empty())
        {
            minimal_effects.apply(conditional_effect.second);
        }
    }
    return minimal_effects;
}

const std::string SAS_Operator::strips_name_to_sas(const std::string &strips_name)
{
    if (strips_name == "<noop-action>")
    {
        return strips_name;
    }
    else
    {
        return strips_name.substr(1, strips_name.size() - 1);
    }
}
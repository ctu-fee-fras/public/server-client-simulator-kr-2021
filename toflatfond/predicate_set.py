import sys

from predicate import Predicate

class PredicateSet:
    
    def __init__(self, init_list=[]):
        self.list = []
        for x in init_list:
            self.add(x)

    def add(self, predicate):
        if isinstance(predicate, Predicate):
            if not any(predicate.can_be_generalized_by(x) for x in self.list):
                self.list = [predicate] + [x for x in self.list if not x.can_be_generalized_by(predicate)]
        else:
            print("Element is not a predicate.", file=sys.stderr)
            exit(1)
    
    def union(self, predicate_set):
        if isinstance(predicate_set, PredicateSet):
            return PredicateSet(self.list + predicate_set.list)
        else:
            print("Given argument is not of class PredicateSet.", file=sys.stderr)
            exit(1)

    def intersection(self, other):
        if isinstance(other, PredicateSet):
            candidates = []
            for predicate in self.list:
                for other_predicate in other.list:
                    if predicate.can_be_generalized_by(other_predicate):
                        candidates.append(predicate)
            for other_predicate in other.list:
                for predicate in self.list:
                    if other_predicate.can_be_generalized_by(predicate):
                        candidates.append(predicate)
            return PredicateSet(candidates)
        else:
            print("Given argument is not of class PredicateSet.", file=sys.stderr)
            exit(1)

    def difference(self, other):
        if isinstance(other, PredicateSet):
            intersection_set = self.intersection(other)
            return PredicateSet([x for x in self.list if x not in intersection_set.list])
        else:
            print("Given argument is not of class PredicateSet.", file=sys.stderr)
            exit(1)
    
    def get_datalist(self, no_and=False):
        if no_and:
            return [x.get_datalist() for x in self.list]
        return ["and"] + [x.get_datalist() for x in self.list]

    def add_prefix(self, prefix):
        for predicate in self.list:
            predicate.name = prefix + predicate.name

    def copy(self):
        return PredicateSet([x.copy() for x in self.list])

    def restore_variable_names(self, predicates):
        for predicate in self.list:
            predicate.restore_variable_names(predicates)
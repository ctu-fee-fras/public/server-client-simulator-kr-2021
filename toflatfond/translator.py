from __future__ import with_statement
import sys
import os
import mypy

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), ".", "fast_downward", "translate")))
import fast_downward.translate.pddl_parser as pddl_parser

from predicate_set import PredicateSet
from predicate import Predicate, create_predicate
import options
import builder

##### USER_SETTINGS #####
output_dir = os.path.join(os.path.dirname(__file__), ".", "flat_domains")
dup_preconds_prefix = "dup-"
act_turn_predicate = "act-round"
event_turn_predicate = "event-round"

##### GLOBAL VARIABLES #####
start_with_event = False

def translate(filename_domain: str, filename_problem: str) -> None:

    print("\nTranslating {} and {} into flatten versions.".format(os.path.basename(filename_domain), os.path.basename(filename_problem)))

    ##### TRANSLATE #####
    parsed_domain_file = pddl_parser.parse_pddl_file("domain", filename_domain)
    parsed_problem_file = pddl_parser.parse_pddl_file("problem", filename_problem)

    options.check_requirements(parsed_domain_file)

    events =        [x 
                        for x in parsed_domain_file 
                            if (type(x) is list) and (x[0] == ":event")
                    ]
    actions =       [x 
                        for x in parsed_domain_file 
                            if (type(x) is list) and (x[0] == ":action")
                    ]
    others =        [x 
                        for x in parsed_domain_file 
                            if (type(x) is not list) or (x[0] not in [":action", ":event"])
                    ]
    
    goal = get_goal(parsed_problem_file)    # single-goal problems only
    objects = get_objects(parsed_problem_file)

    types = get_types(parsed_domain_file)

    action_preconds = parse_action_preconds(actions, objects, types) 
    goal_preconds = parse_goal_preconds(goal, objects, types)
    event_preconds = parse_events_preconds(events, objects, types) 
    
    predicates = get_predicates(parsed_domain_file)

    global act_turn_predicate 
    global event_turn_predicate 
    act_turn_predicate = check_availability(act_turn_predicate, predicates)
    event_turn_predicate = check_availability(event_turn_predicate, predicates)
    
    goal_action_used = PredicateSet(action_preconds + goal_preconds)
    goal_action_used.restore_variable_names(predicates)
    shared = PredicateSet(action_preconds + goal_preconds).intersection(PredicateSet(event_preconds))
    shared.restore_variable_names(predicates)
    print("Shared preconditions found:", end=" ")
    print((shared.list, "none")[len(shared.list) == 0])

    translate_actions(actions, shared, objects, types)
    translate_events(events, shared, goal_action_used, objects, types)

    add_conditional_effect_requirement(others)
    add_shared_predicates(get_predicates(others), shared)

    changed_domain_file = others + actions + create_cond_effect_action(events)
    output_text_domain = builder.pddl_build(changed_domain_file, newline=False, insert_newline_before=[":parameters", ":precondition", ":effect"])

    problem_init = get_init(parsed_problem_file)
    problem_init = translate_init(problem_init, shared, objects, types)
    set_init(parsed_problem_file, problem_init)
    changed_problem_file = parsed_problem_file                              
    output_text_problem = builder.pddl_build(changed_problem_file, newline=False)

    ##### PRINT TRANSLATED TEXT #####
    os.makedirs(output_dir, exist_ok=True)
    output_domain_filename = "flat-" + os.path.basename(filename_domain)
    flatten_file_path = os.path.join(output_dir, output_domain_filename)
    try:
        with open(os.path.join(output_dir, output_domain_filename), 'w') as output_file:
            output_file.write(output_text_domain)    
    except:
        print("Writing to file failed.", file=sys.stderr)
        exit(1)
    print("Flatten domain {} created.".format(os.path.join(output_dir, output_domain_filename)), file=sys.stderr)

    output_problem_filename = "flat-" + os.path.basename(filename_problem)
    flatten_file_path = os.path.join(output_dir, output_problem_filename)
    try:
        with open(os.path.join(output_dir, output_problem_filename), 'w') as output_file:
            output_file.write(output_text_problem)    
    except:
        print("Writing to file failed.", file=sys.stderr)
        exit(1)
    print("Flatten problem {} created.".format(os.path.join(output_dir, output_problem_filename)), file=sys.stderr)

    print("It seems that everything worked.") 


##### TRANSLATOR METHODS #####
def create_cond_effect_action(events: list) -> list:
    cond_eff_action = [":action", "events"] # type: list
    cond_eff_action += [":parameters", []]
    cond_eff_action += [":precondition", ["and", [event_turn_predicate]]]
    cond_eff_action += [":effect", ["and", ["not", [event_turn_predicate]], [act_turn_predicate]] + [create_forall(x) for x in events]] 
    return [cond_eff_action]

def create_forall(event: list) -> list:
    parameters = get_parameters(event)
    preconditions = get_precondition(event)
    effect = get_effect(event)
    return ["forall", parameters, ["when", preconditions, effect]]

"""
    Return reference to ':goal' of given data list.
"""
def get_goal(parsed_problem_file: list) -> list:
    for elem in parsed_problem_file:
        if type(elem) is list and len(elem) > 0 and elem[0] == ":goal":
            return elem
    print("There is no goal in problem file {}.".format(parsed_problem_file),file=sys.stderr)
    exit(1)

def get_pddl_attribute(attribute: str, data_list: list) -> list:
    if attribute in data_list:
        if data_list.index(attribute) + 1 < len(data_list):
            return data_list[data_list.index(attribute) + 1]
    print("Datalist does not contains attribute {}.".format(attribute), file=sys.stderr)
    exit(1)

def set_pddl_attribute(attribute: str, data_list: list, new_value) -> None:
    if attribute in data_list:
        data_list[data_list.index(attribute) + 1] = new_value
    else:
        print("Given data list does not contain {} attribute.".format(attribute), file=sys.stderr)
        exit(1)

"""
    Return reference to preconditions of given data list.    
"""
def get_precondition(data_list: list, fix_and: bool = False) -> list:
    preconditions = get_pddl_attribute(":precondition", data_list)
    if fix_and and len(preconditions) > 0 and preconditions[0] == "and":
        preconditions = preconditions[1:]
    return preconditions

"""
    Return reference to parameters of given data list.
"""
def get_parameters(data_list: list) -> list:
    return get_pddl_attribute(":parameters", data_list)

"""
    Return reference to effect of given data list.    
"""
def get_effect(data_list: list, fix_and: bool = False) -> list:
    effects = get_pddl_attribute(":effect", data_list)
    if fix_and and len(effects) > 0 and effects[0] == "and":
        effects = effects[1:]
    return effects

"""
    Return reference to predicates in given data list.
"""        
def get_predicates(parsed_file: list) -> list:
    for elem in parsed_file:
        if type(elem) is list and len(elem) > 0 and elem[0] == ":predicates":
            return elem
    print("Predicates not found.", file=sys.stderr)
    exit(1)

"""
    Return reference to types in given data list.
"""
def get_types(parsed_file: list) -> list:
    for elem in parsed_file:
        if type(elem) is list and len(elem) > 0 and elem[0] == ":types":
            return elem
    print("Types not found.", file=sys.stderr)
    exit(1)

"""
    Return reference to init in given data list.
"""
def get_init(parsed_file: list) -> list:
    for elem in parsed_file:
        if type(elem) is list and len(elem) > 0 and elem[0] == ":init":
            return elem
    print("Init not found.", file=sys.stderr)
    exit(1)

"""
    In-place modification of data list with ':init'.
"""
def set_init(parsed_file: list, new_init: list) -> None:
    for elem in parsed_file:
        if type(elem) is list and len(elem) > 0 and elem[0] == ":init":
            if len(new_init) > 0 and new_init[0] == ":init":
                elem[1:] = new_init[1:]
                return
            else:
                elem[1:] = new_init
                return
    print("Init not found.", file=sys.stderr)
    exit(1)

"""
    Get objects from parsed data list.
    If argument 'remove_object' is true, leading ':objects' is removed.
"""
def get_objects(parsed_file: list, remove_object: bool =True) -> list:
    for elem in parsed_file:
        if type(elem) is list and len(elem) > 0 and elem[0] == ":objects":
            if remove_object:
                return elem[1:]
            else:
                return elem
    print("Objects not found.", file=sys.stderr)
    exit(1)

"""
    This is working only with "and". Goals with "forall" and other complicated construct are not allowed.
"""
def parse_goal_preconds(data_list: list, objects: list, types: list) -> list:
    predicates = list()
    if type(data_list) is list and data_list[0] == ":goal":
        predicates += [create_predicate(x, [], objects, types) for x in parse_predicates(data_list[1])]
        return predicates
    print("Given datalist is not containing :goal.", file=sys.stderr)
    exit(1)

def parse_action_preconds(actions: list, objects: list, types: list) -> list:
    predicates = list()
    for action in actions:
        parameters = get_parameters(action)
        preconditions = parse_predicates(get_precondition(action))
        for precondition in preconditions:
            predicates.append(create_predicate(precondition, parameters, objects, types))
    return predicates

def parse_events_preconds(events: list, objects: list, types: list) -> list:
    predicates = list()
    for event in events:
        parameters = get_parameters(event)
        preconditions = parse_predicates(get_precondition(event))
        for precondition in preconditions:
            predicates.append(create_predicate(precondition, parameters, objects, types))
    return predicates

"""
    Recursively find predicates in data list.
    Recursive is used for possible nested 'and's in predicates.
"""
def parse_predicates(data_list: list) -> list:
    predicates = list()
    if type(data_list) is list and data_list[0] == "and":
        for elem in data_list[1:]:
            predicates += parse_predicates(elem)
    elif type(data_list) is list:
        predicates.append(data_list)
    return predicates

"""
    In-place inserting :conditional-effects to parsed domain data list.
"""
def add_conditional_effect_requirement(parsed_domain: list) -> None:
    for elem in parsed_domain:
        if type(elem) is list and len(elem) > 0 and elem[0] == ":requirements":
            if ":conditional-effects" not in elem:
                elem.append(":conditional-effects")
            break

def add_shared_predicates(parsed_predicates: list, shared: PredicateSet) -> None:
    dup_shared = shared.copy()
    dup_shared.add_prefix(dup_preconds_prefix)
    for predicate in dup_shared.list:
        parsed_predicates.append(predicate.get_datalist(with_type=True))
    parsed_predicates.append([act_turn_predicate])
    parsed_predicates.append([event_turn_predicate])

def translate_actions(actions: list, shared: PredicateSet, objects: list, types: list) -> None:

    for action in actions:

        parameters = get_parameters(action)
        preconditions = get_precondition(action)
        effects = get_effect(action)

        effect_predicates = [create_predicate(x, parameters, objects, types) for x in effects if x != "and"]

        add_effect_predicates = [x for x in effect_predicates if not x.negative]
        del_effect_predicates = [x for x in effect_predicates if x.negative]

        # predicates from add(a) which can be generalized by at least one predicate from shared
        add_intersection = [x for x in add_effect_predicates if any(x.can_be_generalized_by(y) for y in shared.list)]
        # predicates from del(a) which can be generalized by at least one predicate from shared
        del_intersection = [x for x in del_effect_predicates if any(x.can_be_generalized_by(y) for y in shared.list)]

        for predicate in add_intersection:
            predicate.name = dup_preconds_prefix + predicate.name
            effects.append(predicate.get_datalist())
        for predicate in del_intersection:
            predicate.name = dup_preconds_prefix + predicate.name
            effects.append(predicate.get_datalist())

        add_predicate_to_datalist([act_turn_predicate], preconditions)
        add_predicate_to_datalist(["not", [act_turn_predicate]], effects)
        add_predicate_to_datalist([event_turn_predicate], effects)


def translate_events(events: list, shared: PredicateSet, goal_action_used: PredicateSet, objects: list, types: list) -> None:
    for event in events: 

        parameters = get_parameters(event)
        preconditions = get_precondition(event)
        effects = get_effect(event)

        precondition_predicates = [create_predicate(x, parameters, objects, types) for x in preconditions if x != "and" and isinstance(x, list) and x[0] != "not"] # filter negative preconditions
        effect_predicates = [create_predicate(x, parameters, objects, types) for x in effects if x != "and"]
        add_effect_predicates = [x for x in effect_predicates if not x.negative]
        del_effect_predicates = [x for x in effect_predicates if x.negative]

        # predicates that can be generalized by at least one element from shared are renamed
        for predicate in precondition_predicates:
            if any(predicate.can_be_generalized_by(y) for y in shared.list):
                predicate.name = dup_preconds_prefix + predicate.name
        new_preconditions = ["and"] + [x.get_datalist() for x in precondition_predicates]
        if len(new_preconditions) == 2:  # fix for single predicate preconditions
           set_pddl_attribute(":precondition", event, new_preconditions[1])
        else:
            set_pddl_attribute(":precondition", event, new_preconditions)

        new_del_predicates = [x for x in del_effect_predicates if any(x.can_be_generalized_by(y) for y in goal_action_used.list)]
        new_del_effects = [x.get_datalist() for x in new_del_predicates]

        dup_add_predicates = [x.add_prefix(dup_preconds_prefix) for x in add_effect_predicates if any(x.can_be_generalized_by(y) for y in shared.list)]
        add_difference = [x for x in add_effect_predicates if not any(x.can_be_generalized_by(y) for y in goal_action_used.list)]
        new_add_predicates = add_difference + dup_add_predicates # simple append can suit as union in this situation (there is empty intersection since one set is containing duplicates only)
        new_add_effects = [x.get_datalist() for x in new_add_predicates]
       
        set_effect(event, new_add_effects, new_del_effects)

def translate_init(problem_init: list, shared: PredicateSet, objects: list, types: list) -> list:
    if len(problem_init) > 1 and type(problem_init) is list and problem_init[0] == ":init":    # initial :init fix
        problem_init = problem_init[1:]
        to_duplicate = []   # type: list
        if start_with_event:
            new_init = [":init", [event_turn_predicate]] # TODO: add option to start with events
        else:
            new_init = [":init", [act_turn_predicate]] # TODO: add option to start with events
        last_name = ""
        for parsed_predicate in problem_init: 
            if last_name != parsed_predicate[0]: #insert duplicates when name of previous predicate changes
                last_name = parsed_predicate[0]
                for predicate_to_duplicate in to_duplicate:
                    new_init.append([dup_preconds_prefix + predicate_to_duplicate[0]] + predicate_to_duplicate[1:])
                to_duplicate = []
            predicate = create_predicate(parsed_predicate, [], objects, types)
            if any(predicate.can_be_generalized_by(x) for x in shared.list):
                to_duplicate.append(parsed_predicate)
            new_init.append(parsed_predicate)
        if len(to_duplicate) > 0:
            for predicate_to_duplicate in to_duplicate:
                new_init.append([dup_preconds_prefix + predicate_to_duplicate[0]] + predicate_to_duplicate[1:])
            to_duplicate = []
        return new_init
    else:
        print("Invalid problem_init argument.", file=sys.stderr)
        exit(1)
                    
def set_effect(event: list, add_list: list, del_list: list) -> None:
    new_value = [x for x in add_list + del_list]   
    if len(new_value) > 1: # fix for not-single predicate effects
        new_value = ["and"] + new_value
    elif len(new_value) == 1:
        new_value = new_value[0]
    set_pddl_attribute(":effect", event, new_value)

"""
    Function for fixing possible leading 'and'.
"""
def remove_and(data_list: list) -> list:
    if data_list[0] == "and":
        return remove_and(data_list[1:])
    else:
        return data_list

"""
    In-place predicate insertion into given data list.
"""
def add_predicate_to_datalist(predicate: list, data_list: list) -> None:
    if data_list[0] != "and":
        data_list.append([data_list[0]])
        data_list[0] = "and"
        data_list.append(predicate)
    else:
        data_list.append(predicate)

"""
    Recursively find available predicate_name.
"""
def check_availability(predicate_name: str, parsed_predicates: list) -> str:
    for predicate in parsed_predicates:
        if predicate[0] == predicate_name:
            return check_availability(predicate_name + "-alt", parsed_predicates)
    return predicate_name

if __name__ == "__main__":

    ##### PARSE ARGUMENTS #####
    arguments =  options.parse_args()
    start_with_event = arguments.start_with_event    
    translate(arguments.domain, arguments.problem)
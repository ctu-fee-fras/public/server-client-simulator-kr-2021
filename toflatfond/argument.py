import sys

from type import Type, find_type, find_childs

class Argument:
    
    def __init__(self, text, arg_type):
        self.text = text
        self.type = arg_type

    def __str__(self) -> str:
        return self.text + " - " + self.type.__str__()
    
    def get_datalist(self) -> list:
        return [self.text, "-", self.type.__str__()]

class Constant(Argument):
    def __init__(self, text, arg_type):
        super().__init__(text, arg_type)

    def __eq__(self, other) -> bool:
        if isinstance(other, self.__class__):
            return self.text == other.text and self.type == other.type
        else:
            return False
    
    def __ne__(self, other) -> bool:
        return not self.__eq__(other)

    def copy(self) -> Argument:
        return Constant(self.text, self.type)
    
    def can_be_generalized_by(self, other: Argument) -> bool:
        if isinstance(other, self.__class__):
            return self.__eq__(other)
        elif isinstance(other, Variable):
            return self.type == other.type or other.type.has_descendant(self.type)
        else:
            return False
    
class Variable(Argument):
    def __init__(self, text, arg_type):
        super().__init__(text, arg_type)

    def __eq__(self, other) -> bool:
        if isinstance(other, self.__class__):
            return self.type == other.type
        else:
            return False
    
    def __ne__(self, other) -> bool:
        return not self.__eq__(other)
    
    def copy(self) -> Constant:
        return Constant(self.text, self.type)

    def can_be_generalized_by(self, other: Argument) -> bool:
        if isinstance(other, self.__class__):
            return self.type == other.type or other.type.has_descendant(self.type)
        elif isinstance(other, Constant):
            return False
        else:
            return False

def create_argument(text: str, parameters: list, objects: list, types: list) -> Argument:
    if text[0] == "?":
        type_name = find_type(text, parameters)
        return Variable(text, Type(type_name, types))
    else:
        type_name = find_type(text, objects)
        return Constant(text, Type(type_name, types))
import sys

from argument import create_argument, Variable

class Predicate:

    def __init__(self, name, arguments, negative=False):
        self.name = name
        self.arguments = arguments
        self.negative = negative

    def negate(self):
        self.negative = not self.negative
    
    def get_datalist(self, with_type=False):
        if with_type:
            data_list = [self.name]
            for argument in self.arguments:
                data_list += argument.get_datalist()
        else:
            data_list = [self.name] + [x.text for x in self.arguments]
        if self.negative:
            return ["not", data_list]
        else:
            return data_list

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.name == other.name and self.arguments == other.arguments
        else:
            return False
    
    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        data_list = [self.name] + [x.__str__() for x in self.arguments]
        if self.negative:
            return "(not " + "(" + " ".join(data_list) + "))"
        else:
            return "(" + " ".join(data_list) + ")"
    
    def __repr__(self):
        return self.__str__()

    def is_variable_only(self):
        for argument in self.arguments:
            if not isinstance(argument, Variable):
                return False
        return True
    
    def copy(self):
        return Predicate(self.name, [x.copy() for x in self.arguments], self.negative)

    def can_be_generalized_by(self, other):
        if isinstance(other, self.__class__):
            return self.name == other.name and len(self.arguments) == len(other.arguments) and all([a.can_be_generalized_by(o) for (a, o) in zip(self.arguments, other.arguments)])
        else:
            return False

    def restore_variable_names(self, predicates):
        i = 0
        for argument in self.arguments:
            if isinstance(argument, Variable):
                argument.text = find_variable_name(predicates, self.name, i)
            i += 1
    
    def add_prefix(self, prefix):
        new_predicate = self.copy()
        new_predicate.name = prefix + new_predicate.name
        return new_predicate

def create_predicate(parsed_predicate, parameters, objects, types):
    negative = False
    predicate_body = parsed_predicate
    if type(parsed_predicate) is list and parsed_predicate[0] == "not" and len(parsed_predicate) == 2:
        negative = True
        predicate_body = parsed_predicate[1]
        parsed_predicate = parsed_predicate[1]
    arguments = [create_argument(x, parameters, objects, types) for x in predicate_body[1:]]
    return Predicate(predicate_body[0], arguments, negative)

def find_variable_name(predicates, predicate_name, argument_index):
    if type(predicates) is list and len(predicates) > 0 and predicates[0] == ":predicates":
        predicates = predicates[1:]
    for parsed_predicate in predicates:
        if parsed_predicate[0] == predicate_name:
            arguments_only = [x for x in parsed_predicate if x[0] == "?"]
            if argument_index < len(arguments_only):
                return arguments_only[argument_index]
            else:
                print("Argument index is not valid!", file=sys.stderr)
                exit(1)
    print("Predicate with such name is not in the given list!")
    exit(1)
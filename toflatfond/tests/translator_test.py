import pytest

import translator

def test_set_effect_multiple_add_multiple_del():
    to_change = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", []]
    event = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", ["and", ["pred6"], ["pred7"], ["pred15"], ["not", ["pred11"]], ["not", ["pred10"]]]]
    add_predicates = [["pred6"], ["pred7"], ["pred15"]]
    del_predicates = [["not", ["pred11"]], ["not", ["pred10"]]]
    translator.set_effect(to_change, add_predicates, del_predicates)
    assert to_change == event

def test_set_effect_multiple_add_single_del():
    to_change = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", []]
    event = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", ["and", ["pred6"], ["pred7"], ["not", ["pred11"]]]]
    add_predicates = [["pred6"], ["pred7"]]
    del_predicates = [["not", ["pred11"]]]
    translator.set_effect(to_change, add_predicates, del_predicates)
    assert to_change == event

def test_set_effect_multiple_add_no_del():
    to_change = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", []]
    event = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", ["and", ["pred6"], ["pred7"], ["pred15"]]]
    add_predicates = [["pred6"], ["pred7"], ["pred15"]]
    del_predicates = []
    translator.set_effect(to_change, add_predicates, del_predicates)
    assert to_change == event

def test_set_effect_one_add_multiple_del():
    to_change = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", []]
    event = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", ["and", ["pred6"], ["not", ["pred11"]], ["not", ["pred10"]], ["not", ["pred15"]]]]
    add_predicates = [["pred6"]]
    del_predicates = [["not", ["pred11"]], ["not", ["pred10"]], ["not", ["pred15"]]]
    translator.set_effect(to_change, add_predicates, del_predicates)
    assert to_change == event

def test_set_effect_one_add_one_del():
    to_change = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", []]
    event = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", ["and", ["pred6"], ["not", ["pred11"]]]]
    add_predicates = [["pred6"]]
    del_predicates = [["not", ["pred11"]]]
    translator.set_effect(to_change, add_predicates, del_predicates)
    assert to_change == event

def test_set_effect_one_add_no_del():
    to_change = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", []]
    event = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", ["pred6"]]
    add_predicates = [["pred6"]]
    del_predicates = []
    translator.set_effect(to_change, add_predicates, del_predicates)
    assert to_change == event

def test_set_effect_no_add_multiple_del():
    to_change = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", []]
    event = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", ["and", ["not", ["pred11"]], ["not", ["pred10"]]]]
    add_predicates = []
    del_predicates = [["not", ["pred11"]], ["not", ["pred10"]]]
    translator.set_effect(to_change, add_predicates, del_predicates)
    assert to_change == event

def test_set_effect_no_add_one_del():
    to_change = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", []]
    event = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", ["not", ["pred11"]]]
    add_predicates = []
    del_predicates = [["not", ["pred11"]]]
    translator.set_effect(to_change, add_predicates, del_predicates)
    assert to_change == event

def test_set_effect_no_add_no_del():
    to_change = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", ["pred1"]]
    event = [":event", "event1", ":parameters", [], ":precondition", ["pred1", "pred2 a2 a3", "pred3 a4"], ":effect", []]
    add_predicates = []
    del_predicates = []
    translator.set_effect(to_change, add_predicates, del_predicates)
    assert to_change == event    

def test_add_predicate_to_datalist1():
    predicate = ["p2-alt-alt"]    
    datalist1 = ["and", ["p1"], ["p2-alt"], ["p2-alt-alt"]]
    datalist2 = ["and", ["p1"], ["p2-alt"]] 
    translator.add_predicate_to_datalist(predicate, datalist2)
    assert datalist2 == datalist1

def test_add_predicate_to_datalist2():
    predicate = ["p2-alt"]     
    datalist1 = ["and", ["p1"], ["p2-alt"]] 
    datalist2 = ["p1"]
    translator.add_predicate_to_datalist(predicate, datalist2)
    assert datalist2 == datalist1

def test_check_availability():
    predicates = [["p1"], ["p2-alt"], ["p2-alt-alt"]]
    assert translator.check_availability("p2-alt", predicates) == "p2-alt-alt-alt"
    assert translator.check_availability("p1", predicates) == "p1-alt"
    assert translator.check_availability("p3", predicates) == "p3"
    assert translator.check_availability("p2", predicates) == "p2"

def test_add_conditional_effect_requirement():
    domain = [":domain", "foobar", [":requirements", ":typing", ":conditional-effects"], [":action", "some_name"], "bar", "foo"]
    to_change1 = [":domain", "foobar", [":requirements", ":typing", ":conditional-effects"], [":action", "some_name"], "bar", "foo"]
    to_change2 = [":domain", "foobar", [":requirements", ":typing"], [":action", "some_name"], "bar", "foo"]
    translator.add_conditional_effect_requirement(to_change1)
    translator.add_conditional_effect_requirement(to_change2)
    assert to_change1 == domain and to_change2 == domain

def test_parse_predicates():
    predicates1 = ["and", ["pred1"], ["not", ["pred2"]], ["pred3", "a"]]
    predicates2 = ["and", ["pred1"], ["and", ["not", ["pred2"]], ["pred3", "a"]]] 
    expected = [["pred1"], ["not", ["pred2"]], ["pred3", "a"]]
    result1 = translator.parse_predicates(predicates1)
    result2 = translator.parse_predicates(predicates2)
    assert result1 == expected and result2 == expected

def test_get_objects():
    problem1 = [":problem", "foobar", [":objects", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]], [":action", "some_name"], "bar", "foo"]
    problem2 = [":problem", "foobar", [":objects"], [":action", "some_name"], "bar", "foo"]
    result1 = translator.get_objects(problem1) 
    result2 = translator.get_objects(problem1, False)
    result3 = translator.get_objects(problem2) 
    result4 = translator.get_objects(problem2, False)
    assert result1 == [["pred1", "a1"], ["pred2", "a2"], ["pred3"]]
    assert result2 == [":objects", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]]
    assert result3 == []
    assert result4 == [":objects"]

def test_get_init():
    problem1 = [":problem", "foobar", [":init", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]], [":action", "some_name"], "bar", "foo"]
    problem2 = [":problem", "foobar", [":init"], [":action", "some_name"], "bar", "foo"]
    result1 = translator.get_init(problem1) 
    result2 = translator.get_init(problem2) 
    assert result1 == [":init", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]]
    assert result2 == [":init"]

def test_set_init():
    problem1 = [":problem", "foobar", [":init", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]], [":action", "some_name"], "bar", "foo"]
    problem2 = [":problem", "foobar", [":init", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]], [":action", "some_name"], "bar", "foo"]
    problem3 = [":problem", "foobar", [":init"], [":action", "some_name"], "bar", "foo"]
    problem4 = [":problem", "foobar", [":init"], [":action", "some_name"], "bar", "foo"]
    init1 = [["pred1", "a1"], ["pred2", "a2"], ["pred3"]]
    init2 = []
    translator.set_init(problem3, init1)
    translator.set_init(problem1, init2)
    assert problem3 == problem2 and problem1 == problem4
    translator.set_init(problem2, init2)
    translator.set_init(problem4, init1)
    assert problem4 == problem3 and problem2 == problem1

def test_get_types():
    problem1 = [":problem", "foobar", [":types", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]], [":action", "some_name"], "bar", "foo"]
    problem2 = [":problem", "foobar", [":types"], [":action", "some_name"], "bar", "foo"]
    result2 = translator.get_types(problem2) 
    result1 = translator.get_types(problem1) 
    assert result1 == [":types", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]]
    assert result2 == [":types"]

def test_get_predicates():
    problem1 = [":problem", "foobar", [":predicates", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]], [":action", "some_name"], "bar", "foo"]
    problem2 = [":problem", "foobar", [":predicates"], [":action", "some_name"], "bar", "foo"]
    result2 = translator.get_predicates(problem2) 
    result1 = translator.get_predicates(problem1) 
    assert result1 == [":predicates", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]]
    assert result2 == [":predicates"]

def test_get_effect():
    problem1 = [":problem", "foobar", ":effect", ["and", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]], [":action", "some_name"], "bar", "foo"]
    problem2 = [":problem", "foobar", ":effect", [], [":action", "some_name"], "bar", "foo"]
    result1 = translator.get_effect(problem1) 
    result2 = translator.get_effect(problem1, True) 
    result3 = translator.get_effect(problem2) 
    result4 = translator.get_effect(problem2, True) 
    assert result1 == ["and", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]]
    assert result2 == [["pred1", "a1"], ["pred2", "a2"], ["pred3"]]
    assert result3 == []
    assert result4 == []

def test_get_parameters():
    datalist1 = [":problem", "foobar", ":parameters", ["and", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]], [":action", "some_name"], "bar", "foo"]
    datalist2 = [":problem", "foobar", ":parameters", [], [":action", "some_name"], "bar", "foo"]
    result1 = translator.get_parameters(datalist1)
    result2 = translator.get_parameters(datalist2)
    assert result1 == ["and", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]]
    assert result2 == []

def test_get_preconditions():
    datalist1 = [":problem", "foobar", ":precondition", ["and", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]], [":action", "some_name"], "bar", "foo"]
    datalist2 = [":problem", "foobar", ":precondition", [], [":action", "some_name"], "bar", "foo"]
    result1 = translator.get_precondition(datalist1)
    result2 = translator.get_precondition(datalist2)
    assert result1 == ["and", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]]
    assert result2 == []

def test_get_goal():
    problem1 = [":problem", "foobar", [":goal", ["and", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]]], [":action", "some_name"], "bar", "foo"]
    problem2 = [":problem", "foobar", [":goal", ["pred1", "a1"]], [":action", "some_name"], "bar", "foo"]
    result1 = translator.get_goal(problem1)
    result2 = translator.get_goal(problem2)
    assert result1 == [":goal", ["and", ["pred1", "a1"], ["pred2", "a2"], ["pred3"]]]
    assert result2 == [":goal", ["pred1", "a1"]]


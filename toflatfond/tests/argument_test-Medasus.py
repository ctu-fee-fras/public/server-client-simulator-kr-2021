import pytest

from argument import Variable, Constant, Argument, create_argument
from type import Type

def test_create_argument_variable():
    name = "?a"
    parameters = ":parameters ?a - auv".split(" ")
    objects = "".split(" ")
    types = ":types auv car - vehicle vehicle building - object".split(" ")
    argument = create_argument(name, parameters, objects, types)
    assert \
        isinstance(argument, Variable) \
        and argument.text == name \
        and argument.type == Type("auv", types)

def test_create_argument_constant():
    name = "a"
    parameters = "".split(" ")
    objects = ":objects school - building a - auv superb octavia - vehicle".split(" ")
    types = ":types auv car - vehicle vehicle building - object".split(" ")
    argument = create_argument(name, parameters, objects, types)
    assert \
        isinstance(argument, Constant) \
        and argument.text == name \
        and argument.type == Type("auv", types)

def test_can_be_generalized_by_cc():
    name_a = "a"
    name_b = "b"
    name_octavia = "octavia"
    parameters = ":parameters ?v - vehicle".split(" ")
    objects = ":objects school - building a b - letter superb octavia - vehicle".split(" ")
    types = ":types auv car - vehicle vehicle building letter - object".split(" ")
    argument_a = create_argument(name_a, parameters, objects, types)
    argument_b = create_argument(name_b, parameters, objects, types)
    argument_octavia = create_argument(name_octavia, parameters, objects, types)
    assert \
        not argument_a.can_be_generalized_by(argument_b) \
        and not argument_a.can_be_generalized_by(argument_octavia)

def test_can_be_generalized_by_cv():
    name_a = "a"
    name_octavia = "octavia"
    name_vehicle = "?v"
    parameters = ":parameters ?v - vehicle".split(" ")
    objects = ":objects school - building a b - letter superb octavia - vehicle".split(" ")
    types = ":types auv car - vehicle vehicle building letter - object".split(" ")
    argument_a = create_argument(name_a, parameters, objects, types)
    argument_vehicle = create_argument(name_vehicle, parameters, objects, types)
    argument_octavia = create_argument(name_octavia, parameters, objects, types)
    assert \
        not argument_a.can_be_generalized_by(argument_vehicle) \
        and argument_octavia.can_be_generalized_by(argument_vehicle)

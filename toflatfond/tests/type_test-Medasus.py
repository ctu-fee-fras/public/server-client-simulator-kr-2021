import pytest

from type import Type, find_type, find_childs

"""
    Class Type is well tested through find_childs function.
"""

def test_find_childs_missing_types_prefix():
    to_search = "car"
    search_in = "car - vehicle city road - place"
    childs = find_childs(to_search, search_in.split(" "))
    assert childs == None

def test_find_childs_nonexisting_type():
    to_search = "boat"
    search_in = ":types car - vehicle city road - place"
    childs = find_childs(to_search, search_in.split(" "))
    assert childs == []

def test_find_childs_empty1():
    to_search = "car"
    search_in = ":types car - vehicle city road - place"
    childs = find_childs(to_search, search_in.split(" "))
    assert childs == []

def test_find_childs_empty2():
    to_search = "car"
    search_in = ":types car train"
    childs = find_childs(to_search, search_in.split(" "))
    assert childs == []

def test_find_childs_single1():
    to_search = "vehicle"
    search_in = ":types car - vehicle"
    childs = find_childs(to_search, search_in.split(" "))
    assert \
        len(childs) == 1 \
        and isinstance(childs[0], Type) \
        and childs[0].name == "car"

def test_find_childs_single2():
    to_search = "bug"
    search_in = ":types car - vehicle city road - place ant - bug"    
    childs = find_childs(to_search, search_in.split(" "))
    assert \
        len(childs) == 1 \
        and isinstance(childs[0], Type) \
        and childs[0].name == "ant"

def test_find_childs_multiple1():
    to_search = "bug"
    search_in = ":types boat car - vehicle city - place elephant wasp ant - bug"    
    childs = find_childs(to_search, search_in.split(" "))
    assert \
        len(childs) == 3 \
        and set(["elephant", "wasp", "ant"]) == set(x.name for x in childs) \
        and all(len(x.descendants) == 0 for x in childs)

def test_find_childs_multiple2():
    to_search = "vehicle"
    search_in = ":types boat car - vehicle city - place elephant wasp ant - bug"    
    childs = find_childs(to_search, search_in.split(" "))
    assert \
        len(childs) == 2 \
        and set(["boat", "car"]) == set(x.name for x in childs) \
        and all(len(x.descendants) == 0 for x in childs)

def test_find_childs_multiple_generations():
    to_search = "bug"
    search_in = ":types boat car - vehicle toyota1.6 toyota2.5 - toyota city - place elephant wasp ant - bug blue_wasp red_wasp - wasp toyota nissan - car"
    childs = find_childs(to_search, search_in.split(" "))
    blue_wasp_childs = find_childs("blue_wasp", search_in.split(" "))
    assert \
        len(childs) == 3 \
        and set(["elephant", "wasp", "ant"]) == set(x.name for x in childs) \
        and all(len(x.descendants) == 2 and set(y.name for y in x.descendants) == set(["blue_wasp", "red_wasp"]) for x in childs if x.name == "wasp") \
        and all(len(x.descendants) == 0 for x in childs if x.name != "wasp") \
        and len(blue_wasp_childs) == 0

def test_find_type_valid():
    search_in = "jammed ?v0 ?v1 ?v2 ?v3 - vehicle ?l - location".split(" ")
    assert \
        find_type("?v1", search_in) == "vehicle" \
        and find_type("?l", search_in) == "location" \
        and find_type("?v10", search_in) == None

def test_find_type_invalid():
    search_in = "jammed ?v0 ?v1 ?v2 ?v3 - vehicle ?l - ".split(" ")
    assert \
        find_type("?l", search_in) == None \
        and find_type("?v10", search_in) == None


import pytest

from argument import Variable, Constant, Argument, create_argument
from type import Type

def test_create_argument_variable():
    name = "?a"
    parameters = ":parameters ?a - auv".split(" ")
    objects = "".split(" ")
    types = ":types auv car - vehicle vehicle building - object".split(" ")
    argument = create_argument(name, parameters, objects, types)
    assert isinstance(argument, Variable)
    assert argument.text == name
    assert argument.type == Type("auv", types)

def test_create_argument_constant():
    name = "a"
    parameters = "".split(" ")
    objects = ":objects school - building a - auv superb octavia - car".split(" ")
    types = ":types auv car - vehicle vehicle building - object".split(" ")
    argument = create_argument(name, parameters, objects, types)
    assert isinstance(argument, Constant)
    assert argument.text == name
    assert argument.type == Type("auv", types)

def test_can_be_generalized_by_identities():
    name_a = "a"
    name_vehicle = "?v"
    parameters = ":parameters ?v - vehicle".split(" ")
    objects = ":objects school - building a b - letter superb octavia - car".split(" ")
    types = ":types auv car - vehicle vehicle building letter - object".split(" ")
    argument_a = create_argument(name_a, parameters, objects, types)
    argument_vehicle = create_argument(name_vehicle, parameters, objects, types)
    assert argument_vehicle.can_be_generalized_by(argument_vehicle)
    assert argument_a.can_be_generalized_by(argument_a)

def test_can_be_generalized_by_cc():
    name_a = "a"
    name_b = "b"
    name_octavia = "octavia"
    parameters = ":parameters ?v - vehicle".split(" ")
    objects = ":objects school - building a b - letter superb octavia - car".split(" ")
    types = ":types auv car - vehicle vehicle building letter - object".split(" ")
    argument_a = create_argument(name_a, parameters, objects, types)
    argument_b = create_argument(name_b, parameters, objects, types)
    argument_octavia = create_argument(name_octavia, parameters, objects, types)
    assert not argument_a.can_be_generalized_by(argument_b)
    assert not argument_a.can_be_generalized_by(argument_octavia)

def test_can_be_generalized_by_cv():
    name_a = "a"
    name_octavia = "octavia"
    name_vehicle = "?v"
    parameters = ":parameters ?v - vehicle".split(" ")
    objects = ":objects school - building a b - letter superb octavia - car".split(" ")
    types = ":types auv car - vehicle vehicle building letter - object".split(" ")
    argument_a = create_argument(name_a, parameters, objects, types)
    argument_vehicle = create_argument(name_vehicle, parameters, objects, types)
    argument_octavia = create_argument(name_octavia, parameters, objects, types)
    assert not argument_a.can_be_generalized_by(argument_vehicle)
    assert argument_octavia.can_be_generalized_by(argument_vehicle)

def test_can_be_generalized_by_vv_1():
    name_vehicle = "?v"
    name_auv = "?a"
    name_object = "?o"
    name_highroad = "?h"
    parameters = ":parameters ?a - auv ?o - object ?h - highroad ?v - vehicle".split(" ")
    objects = ":objects school - building a b - letter superb octavia - car".split(" ")
    types = ":types auv car - vehicle school highroad - building vehicle building letter - object".split(" ")
    argument_vehicle = create_argument(name_vehicle, parameters, objects, types)
    argument_auv = create_argument(name_auv, parameters, objects, types)
    argument_object = create_argument(name_object, parameters, objects, types)
    argument_highroad = create_argument(name_highroad, parameters, objects, types)
    assert argument_vehicle.can_be_generalized_by(argument_object)
    assert argument_auv.can_be_generalized_by(argument_object) 
    assert argument_object.can_be_generalized_by(argument_object) 
    assert argument_highroad.can_be_generalized_by(argument_object) 
    assert not argument_vehicle.can_be_generalized_by(argument_highroad) 
    assert not argument_auv.can_be_generalized_by(argument_highroad) 
    assert not argument_object.can_be_generalized_by(argument_highroad) 
    assert argument_highroad.can_be_generalized_by(argument_highroad) 
    assert argument_vehicle.can_be_generalized_by(argument_vehicle) 
    assert argument_auv.can_be_generalized_by(argument_vehicle) 
    assert not argument_object.can_be_generalized_by(argument_vehicle) 
    assert not argument_highroad.can_be_generalized_by(argument_vehicle) 
    assert not argument_vehicle.can_be_generalized_by(argument_auv) 
    assert argument_auv.can_be_generalized_by(argument_auv) 
    assert not argument_object.can_be_generalized_by(argument_auv) 
    assert not argument_highroad.can_be_generalized_by(argument_auv)

def test_can_be_generalized_by_vv_2():
    name_auv = "?a"
    name_car1 = "?c1"
    name_car2 = "?c2"
    parameters = ":parameters ?a - auv ?c1 ?c2 - car".split(" ")
    objects = ":objects school - building a b - letter superb octavia - car".split(" ")
    types = ":types auv car - vehicle school highroad - building vehicle building letter - object".split(" ")
    argument_car1 = create_argument(name_car1, parameters, objects, types)
    argument_auv = create_argument(name_auv, parameters, objects, types)
    argument_car2 = create_argument(name_car2, parameters, objects, types)
    assert argument_auv.can_be_generalized_by(argument_auv)
    assert not argument_auv.can_be_generalized_by(argument_car1) 
    assert not argument_auv.can_be_generalized_by(argument_car2) 
    assert not argument_car1.can_be_generalized_by(argument_auv) 
    assert argument_car1.can_be_generalized_by(argument_car1) 
    assert argument_car1.can_be_generalized_by(argument_car2) 
    assert not argument_car2.can_be_generalized_by(argument_auv) 
    assert argument_car2.can_be_generalized_by(argument_car1) 
    assert argument_car2.can_be_generalized_by(argument_car2)

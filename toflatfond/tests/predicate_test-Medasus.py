import pytest

from predicate import create_predicate

def test_predicate_can_be_generalized_1():
    predicate_text1 = "predicate1 ?a1 ?a2".split(" ")
    predicate_text2 = "predicate1 ?v1 ?v2".split(" ")
    predicate_text3 = "predicate2 ?a1 ?a2".split(" ")
    predicate_text4 = "predicate1 ?c1 ?v2".split(" ")
    parameters = ":parameters ?a1 ?a2 - auv ?c1 - car ?o - object ?h - highroad ?v1 ?v2 - vehicle".split(" ")
    objects = ":objects school - building a b - letter superb octavia - car".split(" ")
    types = ":types auv car - vehicle school highroad - building vehicle building letter - object".split(" ")
    predicate1 = create_predicate(predicate_text1, parameters, objects, types)
    predicate2 = create_predicate(predicate_text2, parameters, objects, types)
    predicate3 = create_predicate(predicate_text3, parameters, objects, types)
    predicate4 = create_predicate(predicate_text4, parameters, objects, types)
    assert \
        predicate1.can_be_generalized_by(predicate2) \
        and not predicate1.can_be_generalized_by(predicate3) \
        and not predicate1.can_be_generalized_by(predicate4) \
        and not predicate2.can_be_generalized_by(predicate1) \
        and not predicate2.can_be_generalized_by(predicate3) \
        and not predicate2.can_be_generalized_by(predicate4) \
        and predicate4.can_be_generalized_by(predicate2)

def test_predicate_can_be_generalized_2():
    predicate_text1 = "predicate1 ?a1 ?a2".split(" ")
    predicate_text2 = "predicate1 ?a1 ?a2 ?a3".split(" ")
    parameters = ":parameters ?a1 ?a2 - auv ?c1 - car ?o - object ?h - highroad ?v1 ?v2 - vehicle".split(" ")
    objects = ":objects school - building a b - letter superb octavia - car".split(" ")
    types = ":types auv car - vehicle school highroad - building vehicle building letter - object".split(" ")
    predicate1 = create_predicate(predicate_text1, parameters, objects, types)
    predicate2 = create_predicate(predicate_text2, parameters, objects, types)
    assert \
        not predicate1.can_be_generalized_by(predicate2) \
        and not predicate2.can_be_generalized_by(predicate1) \


"""
Takes a list from translator and compose it back to pddl format.
Output is partially formatted. For example: no newlines in :types definition.
"""
def pddl_build(parsed_list, ntabs=0, newline=True, insert_newline_before=[]):

    list_of_strings = []

    parent_not = parsed_list[0] == "not"
    newline_bef_ending = parsed_list[0] in [":predicates", ":action"] or parsed_list[0] in ["define", "when", "and", "forall"]

    for element in parsed_list:
        if type(element) is list and len(element) > 0:
            list_of_strings.append(pddl_build(element, ntabs + 1, not parent_not, insert_newline_before))
        elif type(element) is list and len(element) == 0:
            list_of_strings.append("()")
        else:
            if element in insert_newline_before:
                list_of_strings.append("\n" + "\t" * ntabs)
            list_of_strings.append(element)
    
    joined = ("", "\n" + "\t" * ntabs)[newline] + "(" + " ".join(list_of_strings) + ("", "\n" + "\t" * ntabs)[newline_bef_ending] + ")"
    return joined
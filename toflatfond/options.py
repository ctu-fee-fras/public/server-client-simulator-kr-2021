import argparse

import sys

##### OPTIONS #####
unsupported_requirements: list = list(
                                        [":disjunctive-preconditions", 
                                        ":existential-preconditions", 
                                        ":universal-preconditions",
                                        ":quantified-preconditions",
                                        ":conditional-effects",
                                        ":preferences"]
                                     )
needed_requirements:list = list([":typing"])

def check_requirements(parsed_data):
    for elem in parsed_data:
        if type(elem) is list and elem[0] == ":requirements":
            for needed_requirement in needed_requirements:
                if needed_requirement not in elem:
                    print("Domain does not have needed requirement {}.".format(needed_requirement), file=sys.stderr)
                    exit()
            for requirement in elem [1:]:
                if requirement in unsupported_requirements:
                    print("Translator does not support {} requirement.".format(requirement), file=sys.stderr)
                    exit()
                break

def parse_args():

    parser = argparse.ArgumentParser(description='Translator used for translating PDDL domains with events into classical planning with conditional variables.')
    parser.add_argument('domain', type=str,
                        help='path to domain file')
    parser.add_argument('problem', type=str,
                        help='path to problem file')
    parser.add_argument('--start-event', dest='start_with_event', action='store_const',
                        const=True, default=False,
                        help='domain starts with event round (default: False)')

    return parser.parse_args()

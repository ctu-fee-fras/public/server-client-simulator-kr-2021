import builder
import os
import sys

sys.path.append(os.path.abspath(os.path.join(".", "fast_downward", "translate")))
import fast_downward.translate.pddl_parser as pddl_parser

"""
    Script used for formatting single problem file for easier comparison with translator output file.
"""
if __name__ == "__main__":

    filename_domain = "perestroika-domain-flat.pddl"
    filename_problem = "perestroika-problem-flat.pddl"

    parsed_domain_file = pddl_parser.parse_pddl_file("domain", os.path.join("domains", "perestroika", filename_domain))
    output_text = builder.pddl_build(parsed_domain_file, newline=False, insert_newline_before=[":parameters", ":precondition", ":effect"])
    try:
        with open(os.path.join("flat_domains", "formatted-original-" + filename_domain), 'w') as output_file:
            output_file.write(output_text)
    except:
        print("Writing to file failed.", file=sys.stderr)
        exit(1)

    parsed_problem_file = pddl_parser.parse_pddl_file("problem", os.path.join("domains", "perestroika", filename_problem))
    output_text = builder.pddl_build(parsed_problem_file, newline=False)
    try:
        with open(os.path.join("flat_domains", "formatted-original-" + filename_problem), 'w') as output_file:
            output_file.write(output_text)
    except:
        print("Writing to file failed.", file=sys.stderr)
        exit(1)

    filename_domain = "auv-domain-flat.pddl"
    filename_problem = "auv-problem-flat.pddl"

    parsed_domain_file = pddl_parser.parse_pddl_file("domain", os.path.join("domains", "auv", filename_domain))
    output_text = builder.pddl_build(parsed_domain_file, newline=False, insert_newline_before=[":parameters", ":precondition", ":effect"])
    try:
        with open(os.path.join("flat_domains", "formatted-original-" + filename_domain), 'w') as output_file:
            output_file.write(output_text)
    except:
        print("Writing to file failed.", file=sys.stderr)
        exit(1)

    parsed_problem_file = pddl_parser.parse_pddl_file("problem", os.path.join("domains", "auv", filename_problem))
    output_text = builder.pddl_build(parsed_problem_file, newline=False)
    try:
        with open(os.path.join("flat_domains", "formatted-original-" + filename_problem), 'w') as output_file:
            output_file.write(output_text)
    except:
        print("Writing to file failed.", file=sys.stderr)
        exit(1)
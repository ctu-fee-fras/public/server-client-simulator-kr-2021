import sys

class Type:
    def __init__(self, name, types):
        self.name = name
        self.descendants = find_childs(name, types)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.name == other.name
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return self.name

    def add_descendant(self, descendants):
        self.descendants += descendants
    
    def has_descendant(self, descendant):
        if len(self.descendants) > 0:
            for own_descendant in self.descendants:
                if descendant == own_descendant or own_descendant.has_descendant(descendant):
                    return True
            return False
        else:
            return False

def find_type(name, declaration):
    if name in declaration:
        index = declaration.index(name)
        while declaration[index] != "-":
            if index + 1 < len(declaration):
                index += 1
            else:
                print("Invalid parameter declaration!", file=sys.stderr)
                return None
        if index + 1 < len(declaration) and declaration[index + 1] != "":
            return declaration[index + 1]
        else:
            print("Invalid parameter declaration!", file=sys.stderr)
            return None
    print("No predicate {} with such name in the parameters {}!".format(name, declaration), file=sys.stderr)
    return None

def find_childs(name, types):
    if type(types) is list:
        if len(types) > 0 and types[0] == ":types":
            childs = []
            for i in range(1, len(types)):
                if types[i] == name and types[i - 1] == "-":
                    index = i - 2
                    while index > 0 and types[index - 1] != "-":
                        childs.append(Type(types[index], types))
                        index -= 1
                    break
            return childs
        else:
            print("List of types does not begin with ':types' prefix!", file=sys.stderr)
            return None
    else:
        print("Argument 'types' is not list!", file=sys.stderr)
        return None
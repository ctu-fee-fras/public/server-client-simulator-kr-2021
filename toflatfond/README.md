# Translator

Translator used for translating PDDL domains with events into classical planning with conditional variables.

## Usage

To use translator run `translator.py <domain_file> <problem_file> [-h] [--start-event]`.
* `-h` for display script help.
* `--start-event` for begin with event round (event round predicate is added to problem init instead of act round predicate)